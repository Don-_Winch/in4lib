

/*
 *  IN4_LIBRARY
 *  Plugins : jsDelay v1.0.0, jsScrollToMe v1.0.1, jsSteps v1.5.0, jsTabs v2.2.6, jsYandexMaps v1.1.0, jsAutoSize v1.0.1
 *  Date : 13.08.16 18:12:33
 */


"use strict";

var IN4_BASE = (function(AP, is){

	var in4closest = {

		Event : {

			preventDef       : function(e){

				/**
				 * preventDef(ev)
				 *
				 * @param {Event} ev
				 * @return {Undefined}
				 */

				var ev = e || event;
				("preventDefault" in ev) ? ev.preventDefault() : (ev.returnValue = false);
			},

			stopBuble        : function(e){

				/**
				 * preventDef(ev)
				 *
				 * @param {Event} ev
				 * @return {Undefined}
				 */

				var ev = e || event;
				("stopPropagation" in ev) ? ev.stopPropagation() : (ev.cancelBubble = true);

			},

		},

		String : {

			replaceAll       : function(str, search, replace){

				/**
				 * replaceAll(str, search, replace)
				 *
				 * Заменяет все вхождения search внутри str на replace
				 * Возвращает результат
				 *
				 *
				 * @param {String} str - целевая строка
				 * @param {String} search - что заменять
				 * @param {String} replace - чем заменять
				 * @return {String} - результирующая строка
				 */

				var lastIndex = -1, length = search.length, result = str;

				while( ~(lastIndex = str.indexOf(search, lastIndex + 1)) ){

					result = result.replace(
						search, typeof replace == "function" ? replace(lastIndex, search) : replace
					);

				}

				return result;

			},

			replaceAllByList : function(str, obj){

				var result = str, search, replace;

				for(search in obj){

					replace = obj[search];

					result = in4closest.String.replaceAll(result, search, replace);

				}

				return result;

			},

			strRev           : function(str){

				/**
				 * strRev(str)
				 *
				 * Возвращает перевернутую строку
				 *
				 * @param {String} str - целевая строка
				 * @return {String} - результирующая строка
				 */

				var sourceString = String(str),
				    resultString = "",
				    resultLength = sourceString.length
					;

				while(resultLength--){
					resultString += sourceString.charAt(resultLength);
				}

				return resultString;

			},

		},

		Number : {

			nToString        : function(n){

				/**
				 * nToString(n)
				 *
				 * @param {Number} n
				 *
				 * @return {String}
				 */

				return String(n < 10 ? ("0" + n) : n);

			},

			range            : function(min, max, number){

				/**
				 * range(min, max, number)
				 *
				 * Обрезает число, если оно не поподает в диапозон min <-> max
				 *
				 * @param {Number} min - минимальное значение диапозона
				 * @param {Number} max - максимальное значение диапозона
				 * @param {Number} number - целевое число
				 * @return {Number} - результирующее число
				 */

				return Math.max(min, Math.min(number, max));
			},

			isNaN            : function(n){

				return typeof n == "number" && !(n <= Infinity);

			}

		},

		Cookies : {

			getReplacedName : function(name){
				return name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1');
			},

			set : function(name, value, ops){

				/**
				 * set(name, value [,ops])
				 *
				 * @param {String} name
				 * @param {String} value
				 * @param {Object} ops {
				 *      @prop {Number|Boolean} expires,
				 *      @prop {String} domain,
				 *      @prop {String} secure,
				 *      @prop {String|Boolean} path
				 * }
				 *
				 * @return {Undefined}
				 */

				//

				function CookieOptions(){

					/**
					 * new CookieOptions()
					 *
					 * @constructor {CookieOptions}
					 */

					this.data = [];

				}

				CookieOptions.prototype = {

					constructor : CookieOptions,

					set : function(name, value){

						/**
						 * set(name, value);
						 *
						 * @param {String} name
						 * @param {*} value
						 *
						 * @return {Undefined}
						 */

						var curTime;

						switch(name){

							case "path":

								this.push("path", value === true ? "/" : value);

								break;

							case "secure":

								this.push("secure", value);

								break;

							case "domain":

								this.push("domain", value);

								break;

							case "expires":

								curTime = new Date().getTime();
								curTime += (value === true ? 666e10 : value);

								this.push("expires", curTime);

								break;


						}


					},

					push : function(name, value){

						/**
						 * push(name, value)
						 *
						 * @param {String} name
						 * @param {String} value
						 *
						 * @return {Undefined}
						 */

						this.data.push(name + "=" + value);
					},

					build : function(){

						/**
						 * build();
						 *
						 * @return {String}
						 */

						return this.data.length ? (this.data.join("; ") + "; ") : "";
					}

				};

				//

				var cookieName = name,
				    cookieValue = encodeURIComponent(value),
				    cookieOptions = new CookieOptions()
					;

				var innerOptions = in4closest.instanceofBy(Object, ops) ? ops : {};

				var cPath    = ("path" in innerOptions)    && innerOptions.path,
				    cExpires = ("expires" in innerOptions) && innerOptions.expires,
				    cDomain  = ("domain" in innerOptions)  && innerOptions.domain,
				    cSecure  = ("secure" in innerOptions)  && innerOptions.secure
					;

				cPath    && cookieOptions.set("path", cPath);
				cExpires && cookieOptions.set("expires", cExpires);
				cDomain  && cookieOptions.set("domain", cDomain);
				cSecure  && cookieOptions.set("secure", cSecure);

				document.cookie = (cookieName + "=" + cookieValue + "; ") + cookieOptions.build();

			},

			get : function(name){

				/**
				 * get(name);
				 *
				 * @param {String} name
				 *
				 * @return {Null|String}
				 */

				var matches = document.cookie.match(
					new RegExp(
						"(?:^|; )" + this.getReplacedName(name) + "=([^;]*)"
					)
				);

				return matches && decodeURIComponent(matches[1]);


			},

			has : function(name){

				var reg = new RegExp("(^|; )" + this.getReplacedName(name) + "=");

				return reg.test(document.cookie);

			}

		},

		FF               : function(func, context, args){

			/**
			 * FF(func, [,context, args])
			 *
			 * @param {Function} func
			 * @param {*} context
			 * @param {Array} args
			 *
			 * @return {Undefined}
			 */

			typeof func == "function" && func.apply(context, args || undefined);

		},

		evalFunction     : function(argumentsNames, code, context, args){

			/**
			 * evalFunction(argumentsNames, code [, context, arguments])
			 *
			 * Выполняет код code как функцию с аргументами argumentsNames,
			 * передавая ей контекст context и аргументы arguments.
			 * Возвращает результат выполнения.
			 *
			 * @param {String} argumentsNames - имена аргументов (будут доступны по ним изнутри функции), через запятую
			 * @param {String} code - выполняемый внутри функции код
			 * @param {*} context - контекст, передаваемый в функцию
			 * @param {Array} args - массив аргументов, передаваемых в функцию
			 * @return {*} - результат выполнения функции
			 */


			try{

				return new Function(argumentsNames, code).apply(context || window, args);

			}catch(e){

				throw new Error("Ошибка в выражении: " + code);

			}
		},

		instanceofBy     : function(constructor, target){

			/**
			 * instanceofBy(constructor, target)
			 *
			 * Более правильная проверка на принадлежность target к конструктору constructor
			 *
			 * @param {Function} constructor - конструктор
			 * @param {Object} target - проверяемый экземпляр
			 * @return {Boolean} - true, если target создан через constructor
			 */


			return constructor !== target && target instanceof constructor;
		},

		require          : function(src, options){

			/**
			 *
			 * require(src [,options])
			 *
			 * @param {String} src
			 * @param {Object} options {
			 *      @prop {String} method
			 *      @prop {Boolean} async
			 *      @prop {Boolean} defer
			 *      @prop {Boolean} unique
			 * }
			 *
			 */


			function requireSync(path, defer, async){

				/**
				 * requireSync(path, defer, async)
				 *
				 * @param {String} path
				 * @param {Boolean} defer
				 * @param {Boolean} async
				 *
				 * @return {Undefined}
				 */

				var attributes = ["", "type='text/javascript'"];

				attributes.push("src" + "=" + "'" + path + "'");

				defer && attributes.push("defer");
				async && attributes.push("async");

				document.write("<scr" + "ipt" + attributes.join(" ") + "></scr" + "ipt>");

			}

			function requireAsync(path){

				/**
				 * requireAsync(path)
				 *
				 * @param {String} path
				 *
				 * @return {Undefined}
				 */

				var script = document.createElement("script");

				script.src   = path;
				script.type  = "text/javascript";

				document.getElementsByTagName("head")[0].appendChild(script);

			}

			function findScript(path){

				/**
				 * findScript(path)
				 *
				 * @param {String} path
				 *
				 * @return {Boolean}
				 */

				var scripts = document.getElementsByTagName("script");

				return Array.prototype.slice.call(scripts).some(function(script){
					return script.src == path;
				});

			}

			var innerOptions = $.extend(
				{
					method : "append",
					async  : false,
					defer  : false,
					unique : true
				},
				options
			);

			if(!innerOptions.unique || !findScript(src)){

				innerOptions.method == "append"
					? requireAsync(src)
					: requireSync(src, innerOptions.defer, innerOptions.async)
				;

			}



		},

		requestAnimation : function(fnc){

			/**
			 * requestAnimation(fnc)
			 *
			 * requestAnimationFrame для старых браузеров
			 *
			 * @param {Function} fnc - целевая функция
			 * @return {Undefined}
			 */

			is["animation"] ? requestAnimationFrame(fnc) : setTimeout(fnc, 1);

		},

		promise          : (function(){

			/**
			 *
			 * promise(func [,noStart]);
			 *
			 * @param {Function} func - ожидаемая функция. Запускается как func(Promise)
			 * @param {Boolean} noStart - true, если не нужно запускать обещание
			 *
			 * @return {Object} - обещание (Promise)
			 *
			 */

			var   STATUS_PENDING = "pending"
				, STATUS_REJECT  = "rejected"
				, STATUS_RESOLVE = "resolved"
				, STATUS_STARTED = "started"
				, STATUS_WAIT    = "waiting"
				;

			function f_add(t, f){

				/**
				 * f_add(t, f)
				 *
				 * @param {*} f
				 * @param {Array} t
				 *
				 * @return {Undefined}
				 */

				typeof f == "function" && t.push(f);
			}

			function _Promise(f){

				/**
				 *
				 * new _Promise(f [,noStart]);
				 *
				 * @constructor {_Promise}
				 *
				 * @param {Function} f
				 *
				 */

				this.watcher   = f;
				this.status    = STATUS_WAIT;
				this._handlers = {
					resolved  : [],
					rejected  : []
				};


			}

			_Promise.prototype = {
				constructor : _Promise,
				setDone : function(success){

					/**
					 *
					 * setDone(success);
					 *
					 * Устанавливает обработчик на переход в состояние resolved
					 *
					 * @param {Function} success
					 *
					 * @return {_Promise} - this
					 *
					 */

					return f_add(this._handlers.resolved, success), this;
				},
				setFail : function(failed){

					/**
					 *
					 * setFain(failed);
					 *
					 * Устанавливает обработчик на переход в состояние rejected
					 *
					 * @param {Function} failed
					 *
					 * @return {_Promise} - this
					 *
					 */

					return f_add(this._handlers.rejected, failed), this;
				},
				resolve : function(message){

					/**
					 *
					 * resolve(message);
					 *
					 * Переводит обещание в состояние "resolved".
					 * Вызывает обработчики, как func(Promise, message);
					 *
					 * @param {*} message
					 *
					 * @return {_Promise} - this
					 *
					 */

					var self = this;

					if(self.status == STATUS_PENDING){

						self.close(STATUS_RESOLVE);

						self._handlers.resolved.forEach(function(f){
							f(self, message);
						});

					}

					return this;

				},
				reject : function(error){

					/**
					 *
					 * reject(error);
					 *
					 * Переводит обещание в состояние "rejected".
					 * Вызывает обработчики, как func(Promise, error);
					 *
					 * @param {*} error
					 *
					 * @return {_Promise} - this
					 *
					 */

					var self = this;

					if(self.status == STATUS_PENDING){

						self.close(STATUS_REJECT);

						self._handlers.rejected.forEach(function(f){
							f(self, error);
						});

					}

					return this;

				},
				close  : function(type){

					/**
					 *
					 * close(type);
					 *
					 * Переводит обещание в состояние type, не вызывая никаких обработчиков.
					 *
					 * @param {String} type
					 *
					 * @return {_Promise} - this
					 *
					 */

					this.status = type;

					return this;

				},
				removeHandlers : function(type, func){

					/**
					 *
					 * removeHandlers(type [,func]);
					 *
					 * Удаляет все или конкретный обработчик, соответствующий типу type
					 *
					 * @param {String} type - тип обработчика (к примеру, resolved/rejected)
					 * @param {Function} func - искомая функция-обработчик
					 *
					 * @return {_Promise} - this
					 *
					 */

					var targetArray, targetIndex, targetLength;

					if(type in this._handlers){
						targetArray  = this._handlers[type];
						targetIndex  = 0;
						targetLength = targetArray.length;
					}

					if(targetArray){

						if(typeof func == "function"){
							targetIndex  = targetArray.indexOf(func);
							targetLength = 1;
						}

						targetArray.splice(targetIndex, targetLength);

					}

					return this;

				},
				start : function(f){

					/**
					 *
					 * start(f);
					 *
					 * Запускает обещание
					 *
					 * @param {Function} f - ожидаемая функция
					 *
					 * @return {_Promise} - this
					 *
					 */

					this.status = STATUS_PENDING;
					this.watcher = f || this.watcher;

					try{
						this.watcher(this);
					}catch(e){
						this.reject(e);
					}

					return this;

				},
				restart : function(removeHandlers){

					/**
					 *
					 * start(f);
					 *
					 * Перезапускает обещание с возможным удалением обработчиков
					 *
					 * @param {Boolean} removeHandlers - true, если нужно удалить все обработчики
					 *
					 * @return {_Promise} - this
					 *
					 */

					this.close(STATUS_STARTED);

					if(removeHandlers){
						this.removeHandlers();
					}

					return this.start(this.watcher);
				}
			};

			//////////////////////////

			function _init(f){

				/**
				 * _init(f)
				 *
				 * @param {Function} f
				 *
				 * @return {_Promise}
				 */

				return new _Promise(f);
			}

			_init.when = function(arr){

				/**
				 *
				 * when([arr]);
				 *
				 * @param {Array} arr
				 *
				 * @return {_Promise}
				 *
				 */

				var args       = [],
				    argsLength = 0
					;

				args = arr.filter(function(prom){
					return in4closest.instanceofBy(_Promise, prom);
				});

				return new _Promise(function(self){

					if(argsLength = args.length){

						args.forEach(function(promise){

							promise
								.setDone(function(){
									--argsLength || self.resolve();
								})
								.setFail(function(_){
									argsLength = 0;
									self.reject(_);
								})
								.start()
							;

						});

					}else{

						self.resolve(self);

					}

				});

			};

			return _init;

		})(),

		sliceArguments   : function(){

			/**
			 * sliceArguments.apply(start [, arguments])
			 *
			 * @param {Arguments} arguments - псевдомассив аргументов
			 * @param {Number} start - позиция псевдомассива, с которой начинается сборка
			 *
			 * @return {Array} - результирующий массив
			 */

			return AP.slice.call(arguments, this);
		},

		stopwatch        : {

			/**
			 * stopwatch
			 *
			 * Обертка для console.time, console.timeEnd
			 *
			 */

			data : {},

			start : function(name){

				/**
				 * stopwatch.start(name)
				 *
				 * Аналог console.time(name)
				 *
				 * @param {String} name - идентификатор таймера
				 * @return {Undefined}
				 */

				var date, collection = this.data;

				if(is["consoleTime"]){

					console.time(name);

				}else if(is["consoleLog"]){

					date = (new Date()).getTime();

					console.log("Таймер " + name + " запущен");
					collection[name] = date;

				}

			},
			end : function(name){

				/**
				 * stopwatch.end(name)
				 *
				 * Аналог console.timeEnd(name)
				 *
				 * @param {String} name - идентификатор таймера
				 * @return {Undefined}
				 */

				var date, collection = this.data;

				if(is["consoleTime"]){

					console.timeEnd(name);

				}else if(is["consoleLog"] && name in collection){

					date = (new Date()).getTime() - collection[name];

					console.log("Отчет таймера " + name + ": " + date + "мс");

					delete collection[name];

				}

			}
		},

		addPlugin        : function (pluginName, options, initializer){

			/**
			 * addPlugin(pluginName, options, initializer)
			 *
			 * "Моторчик" для создания плагинов jQuery.
			 * jQuery.fn[pluginName] = initializer(options);
			 * jQuery.fn[pluginName][optionName] = options[optionName] (если optionName не начинается с "_");
			 *
			 * @param {String} pluginName
			 * @param {Object} options
			 * @param {Function} initializer
			 *
			 * @return {Boolean} - true, если плагин был добавлен
			 */


			if(typeof jQuery == "undefined"){
				return false;
			}

			//

			var jQueryConstructor = initializer(options), optionName, jQueryWrapper = function(){
				return jQueryConstructor.apply(this, arguments);
			};

			for(optionName in options){

				if( optionName.charAt(0) != "_" ){
					jQueryWrapper[optionName] = options[optionName];
				}

			}

			jQuery.fn[pluginName] = jQueryWrapper;

			return true;


		},

		buildPlugins     : (function(_plugList){

			/**
			 * buildPlugins(context)
			 *
			 * Инициализирует все плагины из коллекции
			 * &.add - добавление плагина
			 * &.drop - удаление плагина
			 * &.use - инициализация плагина
			 * &.is - проверка на существование плагина
			 * &.info - информация о плагине
			 * &.list - список плагинов
			 * &.get - получение объекта плагина
			 * &.newHandler, &.removeHandler - установка и удаление обработчиков
			 *
			 * @param {*} context - контекст, передаваемый в функцию-сборщик
			 * @return {Undefined}
			 */


			var   ERROR_NOT_FOUND = "Данный плагин не обнаружен"
				, ERROR_NOT_DESC  = "Описание к данному плагину отсутствует"
				, ERROR_YEP_FOUND = "Такой плагин уже существует!"
				, ERROR_NOT_CONSTRUCTOR = "У вашего плагина нет движка! (Метода constructor)"
				;

			//////////////////////

			function PluginUnit(name, description, constructor){

				/**
				 * new PluginUnit(name, description, constructor)
				 *
				 * @constructor {PluginUnit}
				 *
				 * @param {String} name
				 * @param {String} description
				 * @param {Function} constructor
				 *
				 * @return {Undefined}
				 */

				this.name = name;
				this.picker = constructor;
				this.description = description;
				this.handlers = {};
			}

			PluginUnit.prototype = {
				constructor: PluginUnit,
				execute : function(context){

					/**
					 * execute([context])
					 *
					 * Вызывает сборщик и обработчики "after", "before"
					 * Передает context них, в качетсве обычного аргумента
					 *
					 * @param {*} context
					 *
					 * @return {Undefined}
					 */

					var data = [context];

					this.triggerHandler("before", data);

					this.picker.apply(this, data);

					this.triggerHandler("after", data);


				},
				setHandler : function(handlerName, handlerFunc){

					/**
					 * setHandler(handlerName, handlerFunc)
					 *
					 * Устанавливает обработчик
					 *
					 * @param {String} handlerName
					 * @param {Function} handlerFunc
					 *
					 * @return {Undefined}
					 */

					var handlers = this.handlers;

					if(!(handlerName in handlers)){
						handlers[handlerName] = [];
					}

					handlers[handlerName].push(handlerFunc);

				},
				removeHandler : function(handlerName, handlerFunc){

					/**
					 * removeHandler(handlerName, handlerFunc)
					 *
					 * Удаляет обработчик
					 *
					 * @param {String} handlerName
					 * @param {Function} handlerFunc
					 *
					 * @return {Undefined}
					 */

					if(!(handlerName in this.handlers)){
						return;
					}

					var handlerUnit = this.handlers[handlerName], i = handlerUnit.length;

					while(i--){

						if(handlerUnit[i] === handlerFunc){
							handlerUnit.splice(i++, 1);
						}

					}

				},
				triggerHandler : function(handlerName, args){

					/**
					 * triggerHandler(handlerName, args)
					 *
					 * Вызывает обработчики
					 *
					 * @param {String} handlerName
					 * @param {Array} args - аргументы, передаваемые в обработчик
					 *
					 * @return {Undefined}
					 */

					var self = this;

					if(handlerName in self.handlers){

						self.handlers[handlerName].forEach(function(f){
							f.apply(self, args);
						});

					}


				}
			};

			///////////////////////


			function _plugInit(context){

				/**
				 * _plugInit(context)
				 *
				 * @param {*} context
				 *
				 * @return {Undefined}
				 */

				_plugInit.use(context, _plugInit.list());

			}

			_plugInit.plugins = _plugList;
			_plugInit.is = function(plugName){

				/**
				 * buildPlugins.is(plugName)
				 *
				 * Проверяет существование плагина
				 *
				 * @param {String} plugName
				 *
				 * @return {Boolean}
				 */

				return (plugName in _plugList);
			};
			_plugInit.use = function(context, plugNames){

				/**
				 * buildPlugins.use(context, plugNames)
				 *
				 * Инициализирует плигны из plugNames
				 *
				 * @param {*} context - первый аргумент, передаваемый в функцию-сборщик
				 * @param {String|Array} plugNames - идентификатор или массив идентификаторов плагинов, которые нужно запустить
				 *
				 * @return {Undefined}
				 */


				function eachNames(name){

					if(_plugInit.is(name)){

						in4closest.requestAnimation(function(){
							_plugList[name].execute(context);
						});

					}

				}

				in4closest.instanceofBy(Array, plugNames) ? plugNames.forEach(eachNames) : eachNames(plugNames);

			};
			_plugInit.info = function(plugName){

				/**
				 * buildPlugins.info(plugName)
				 *
				 * Возвращает информацию о плагине
				 *
				 * @param {String} plugName - идентификатор целевого плагина
				 * @return {String} - информация о плагине
				 */


				if(_plugInit.is(plugName)){

					return _plugList[plugName].description || ERROR_NOT_DESC;

				}else{

					throw new Error(ERROR_NOT_FOUND);

				}

			};
			_plugInit.get = function(plugName){

				/**
				 * buildPlugins.get(plugName)
				 *
				 * Возваращает PluginUnit-объект
				 *
				 * @param {String} plugName - идентификатор целевого плагина
				 *
				 * @return {PluginUnit}
				 */

				return _plugInit.is(plugName) && _plugList[plugName];

			};
			_plugInit.add = function(plugName, plugBox){

				/**
				 * buildPlugins.add(plugName, plugBox)
				 *
				 * Добавляет плагин в коллекцию
				 *
				 * @param {String} plugName - идентификатор нового плагина
				 * @param {Object} plugBox  - тело плагина {
				 *      @prop {Function} constructor - функция-сборщик. Вызывается как constructor.call(PlugUnit, context, argument) (см. buildPlugins.use),
				 *      @prop {String} description - описание плагина
				 * }
				 *
				 * @return {PluginUnit}
				 */

				if(_plugInit.is(plugName)){

					throw new Error(ERROR_YEP_FOUND);

				}else if(typeof plugBox.constructor == "function"){

					_plugList[plugName] = new PluginUnit(plugName, plugBox.description, plugBox.constructor);

				}else{

					throw new Error(ERROR_NOT_CONSTRUCTOR);

				}

			};
			_plugInit.list = function(){

				/**
				 * buildPlugins.list()
				 *
				 * Возвращает список плагинов в коллекции
				 *
				 * @return {Array} - массив идентификаторов
				 */

				//without Object.keys

				var keyBox = [], key;

				for(key in _plugList){
					keyBox.push(key);
				}

				return keyBox;
			};
			_plugInit.drop = function(plugName){

				/**
				 * buildPlugins.drop(plugName)
				 *
				 * Удаляет плагин из коллекции
				 *
				 * @return {Boolean} - true, если не произошло ошибки
				 */

				return _plugInit.is(plugName) && (delete _plugList[plugName]);

			};
			_plugInit.newHandler = function(plugName, handlerName, handlerFunc){

				/**
				 * buildPlugins.newHandler(plugName, handlerName, handlerFunc)
				 *
				 * Установка обработчика
				 *
				 * @param plugName - идентификатор плагина
				 * @param handlerName - название обработчика
				 * @param handlerFunc - функция обработчика
				 *
				 * @return {Undefined}
				 */

				if(_plugInit.is(plugName)){

					_plugList[plugName].setHandler(handlerName, handlerFunc);

				}else{

					throw new Error(ERROR_NOT_FOUND);

				}

			};
			_plugInit.removeHandler = function(plugName, handlerName, handlerFunc){

				/**
				 * buildPlugins.removeHandler(plugName, handlerName, handlerFunc)
				 *
				 * Удаление обработчика
				 *
				 * @param plugName - идентификатор плагина
				 * @param handlerName - название обработчика
				 * @param handlerFunc - функция обработчика
				 *
				 * @return {Undefined}
				 */

				if(_plugInit.is(plugName)){

					_plugList[plugName].removeHandler(handlerName, handlerFunc);

				}else{

					throw new Error(ERROR_NOT_FOUND);

				}


			};

			return _plugInit;

		})({}),

		version : "3.1.0"

	};

	return in4closest;

})(
	Array.prototype, {
		"animation"   : ("requestAnimationFrame" in window),
		"consoleTime" : ("time" in console),
		"consoleLog"  : ("log" in console)
	}
);


!function($, IN4_BASE){


	/**
	 * $jQElem.jsDelay(time, func)
	 *
	 * Более удобный setTimeout для jQuery
	 *
	 * @param {Number} time - время, через которое сработает таймер
	 * @param {Function} func - функция, вызываемая по истечению времени time. Вызывается как func.call($jQElem)
	 * @return {jQuery} - $jQElem
	 */

	!IN4_BASE.addPlugin("jsDelay",
		{
			defaultOptions : { delay : 1 }
		},
		function(_ops){

			return function(time, f){

				var $targets = this;

				setTimeout(function(){
					f.call($targets);
				}, time || _ops.defaultOptions.delay);

				return this;
			}

		}
	);

	/**
	 * $jQElem.jsScrollToMe([px, time, container])
	 *
	 * Прокруччивает container к верху элемента $jQElem минус px за время time
	 *
	 * @param {Number} px - требуемый отступ от верхней границы окна до блока
	 * @param {Number} time - скорость выполнения прокрутки
	 * @param {String|Element} container - селектор для поиска или ссылка на элемент, который будем прокручивать
	 * @return {jQuery} - $jQElem
	 */

	!IN4_BASE.addPlugin("jsScrollToMe",
		{
			defaultOptions : {
				indent : 10,
				speed  : 500,
				targetSelector : "body, html"
			}
		},
		function(_ops){

			return function(px, time, selector){

				var offset = this.offset(), needOffset, target;

				if(offset){

					needOffset = offset.top - (px || _ops.defaultOptions.indent);
					target     = selector || _ops.defaultOptions.targetSelector;

					$(target).scrollTop() === needOffset || $(target).animate(
						{ scrollTop : needOffset },
						time || _ops.defaultOptions.speed
					);

				}

				return this;

			}

		}
	);

	/**
	 * $jQElem.jsSteps([options])
	 *
	 * Компонент "Steps" (Шаги)
	 *
	 * @param {Object} options - настройки плагина {
	 *      @prop {String} targetSelector - селектор целевых дочерних элементов,
	 *      @prop {String} color - цвет прогресс-бара,
	 *      @prop {Number} startStep - стартовый шаг,
	 *      @prop {Boolean} notTriggeredStart - если true, то при инициализации не будут вызваны обработчики beforeChange и afterChange,
	 *      @prop {String} nextButton - селектор кнопки "вперед",
	 *      @prop {String} prevButton - селектор кнопки "назад",
	 *      @prop {Function} condition - функция, вызываемая при переходе к другому шагу, должна возвращать true, если разрешает переход.
	 * }
	 *
	 * @event - beforeCheck.call($jQElem#get, controllerObject, curStepObject, nextStepObject, isEmulation)  - перед проверкой options.condition
	 * @event - beforeChange.call($jQElem#get, controllerObject, curStepObject, nextStepObject, isEmulation) - перед переключением щага
	 * @event - afterChange.call($jQElem#get, controllerObject, prevStepObject, curStepObject, isEmulation)  - после переключения щага
	 * @return {jQuery} - $jQElem
	 */

	!IN4_BASE.addPlugin("jsSteps",
		{
			defaultOptions : {
				targetSelector    : "*",
				color             : "#000000",
				startStep         : 0,
				notTriggeredStart : false,
				nextButton : "a[href='#js-Steps_next']",
				prevButton : "a[href='#js-Steps_prev']"
			}
		},
		function(_ops){

			var   CLASS_BODY = "js-Steps_body"
				, CLASS_LINE = "js-Steps_line"
				, CLASS_LINE_HEADER = "js-Steps_lineHeader"
				, CLASS_LINE_CURRENT = "js-Steps_lineCurrent"
				, CLASS_LINE_PROGRESS = "js-Steps_lineProgress"
				, CLASS_LINE_TRIGGER = "js-Steps_lineTrigger"
				, TAG_BODY = "<div />"
				, TAG_LINE = "<div />"
				, TAG_LINE_HEADER = "<h2 />"
				, TAG_LINE_CURRENT = "<span />"
				, TAG_LINE_PROGRESS = "<div />"
				, TAG_LINE_TRIGGER = "<div />"
				, CLASS_STEP = "js-Steps_target"
				, CLASS_STEP_DISABLED = "disabled"
				, CLASS_STEP_ACTIVE  = "enabled"
				, TRIGGER_PREFIX = "jsSteps"
				, TRIGGER_BEFORE_CHANGE = "beforeChange"
				, TRIGGER_AFTER_CHANGE = "afterChange"
				, TRIGGER_BEFORE_CHECK = "beforeCheck"
				, DATA_SAVE_CONTAINER = "jsSteps"
				;

			function CreateStepLine(container, $steps, color, condition, callBack){

				/**
				 * new CreateStepLine(container, $steps, color, condition, callBack)
				 *
				 * @constructor
				 * @this {CreateStepLine}
				 *
				 * @param {Element} container
				 * @param {jQuery} $steps
				 * @param {String} color
				 * @param {Function} condition
				 * @param {Function} callBack
				 */

				var $stepBody          = $(TAG_BODY).addClass(CLASS_BODY),
				    $stepTop           = $(TAG_LINE).addClass(CLASS_LINE),
				    $stepTop_Header    = $(TAG_LINE_HEADER).addClass(CLASS_LINE_HEADER),
				    $stepTop_Number    = $(TAG_LINE_CURRENT).addClass(CLASS_LINE_CURRENT),
				    $stepTop_Line      = $(TAG_LINE_PROGRESS).addClass(CLASS_LINE_PROGRESS),
				    $stepTop_Progress  = $(TAG_LINE_TRIGGER)
					    .addClass(CLASS_LINE_TRIGGER)
					    .css("background", color)
					;

				//// Все аппенды ///

				$stepTop_Line.append($stepTop_Progress);

				$stepTop_Header
					.append($stepTop_Number)
					.append("<span>шаг</span>")
				;

				$stepTop
					.append($stepTop_Header)
					.append($stepTop_Line)
				;

				$steps
					.addClass(CLASS_STEP + " " + CLASS_STEP_DISABLED)
					.appendTo($stepBody)
				;

				/////////////////////////////////////

				this.currentStep = null;
				this.stepCount   = $steps.length;
				this.callBack   = callBack;
				this.condition  = condition;
				this.containers  = {
					header : $stepTop.get(0),
					body   : $stepBody.get(0),
					main   : container
				};
				this.controllers = {
					number : $stepTop_Number.get(0),
					line   : $stepTop_Progress.get(0),
					steps  : $steps.get()
				};

				/////////////////////////////////////

			}

			CreateStepLine.prototype = {

				toStep : function(number, notTriggered, emulation, withoutCondition){

					/**
					 * toStep(number [,notTriggered, emulation, withoutCondition]);
					 *
					 * @param {Number} number - номер шага
					 * @param {Boolean} notTriggered - true, если не нужно вызывать обработчики
					 * @param {Boolean} emulation - true, если функция вызвана программно
					 * @param {Boolean} withoutCondition - true, если не требуется проверка CreateStepLine.condition
					 *
					 * @return {Undefined}
					 */

					var newStepNumber = IN4_BASE.Number.range(0, this.stepCount - 1, number),
					    oldStepNumber = this.currentStep,
					    newStepElement = this.controllers.steps[newStepNumber],
					    oldStepElement = this.controllers.steps[oldStepNumber],
					    newVisualStepNumber  = newStepNumber + 1,
					    newVisualStepWidth   = Math.min(100, Math.ceil(100 / this.stepCount) * newVisualStepNumber)
						;

					var triggerData = [
						this,
						{
							index   : oldStepNumber,
							element : oldStepElement
						},
						{
							index   : newStepNumber,
							element : newStepElement
						},
						emulation || false
					];

					var allowStep  =  withoutCondition ||
						    typeof this.condition == "function" &&
						    this.condition.apply(this.containers.main, triggerData)
						;

					notTriggered || this.callBack(TRIGGER_BEFORE_CHECK, triggerData);

					if(newStepNumber !== oldStepNumber && allowStep){

						notTriggered || this.callBack(TRIGGER_BEFORE_CHANGE, triggerData);

						$(newStepElement)
							.add(oldStepElement)
							.toggleClass(CLASS_STEP_ACTIVE + " " + CLASS_STEP_DISABLED)
						;

						$(this.controllers.number).text(newVisualStepNumber);
						$(this.controllers.line).css("width", newVisualStepWidth + "%");

						this.currentStep = newStepNumber;

						notTriggered || this.callBack(TRIGGER_AFTER_CHANGE, triggerData);

					}


				}

			};

			return function(options){

				var innerOptions = $.extend({}, _ops.defaultOptions, options);

				return this.each(function(){

					var handlerID = "jsSteps" + Math.floor(Math.random() * 2e10);

					var  self  = this,
					     $self  = $(this),
					     $steps = $self.children(innerOptions.targetSelector)
						;

					var engine = new CreateStepLine(
						this, $steps, innerOptions.color, innerOptions.condition,
						function(name, data){
							$(self).trigger(name + "." + TRIGGER_PREFIX, data.slice());
						}
					);

					engine.toStep(innerOptions.startStep, innerOptions.notTriggeredStart, true, true);

					$self
						.on(handlerID, function(e, step){
							engine.toStep(step);
						})
						.on("click", innerOptions.nextButton, function(e){

							e.preventDefault();
							e.stopPropagation();

							$(this).trigger(handlerID, [engine.currentStep + 1]);

						})
						.on("click", innerOptions.prevButton, function(e){

							e.preventDefault();
							e.stopPropagation();

							$(this).trigger(handlerID, [engine.currentStep - 1]);

						})
						.data(DATA_SAVE_CONTAINER, engine)
						.prepend(engine.containers.body)
						.prepend(engine.containers.header)
					;

				});

			}

		}
	);

	/**
	 * $jQElem.jsTabs([options, ..args])
	 *
	 * Компонент "Tabs" (Вкладки)
	 *
	 * @param {Object|String} options - имя метода (потомка _JSTabsConstructor) или настройки плагина {
	 *      @prop {String} menuSelector - СSS-селектор переключателей,
	 *      @prop {String} menuAttribute - HTML-атрибут переключателя, содержаший селектор цели,
	 *      @prop {String|Function} blockContext - Элемент, в котором лежат целевые блоки (если функция, то вызывается перед переключением таба как blockContext(menuItem, targetSelector) и должна возвращать элемент или селектор),
	 *      @prop {String} menuClassActive - Класс, устанавливаемый активному табу,
	 *      @prop {String} menuAttribute - HTML-атрибут переключателя, содержаший селектор цели,
	 *      @prop {String} blockClassActive - Класс, устанавливаемый активному целевому блоку,
	 *      @prop {String} blockClassVisible - Класс, устанавливаемый активному целевому блоку спустя 100мс после активации,
	 *      @prop {Boolean} preventDef - нужна ли отмена дефолтного действия браузера при переключении табов,
	 *      @prop {Boolean} notFirstActive - если не нужно активировать 1 из табов при инициализации,
	 *      @prop {String} event - событие для активации таба ("click", "mouseover" и т.п)
	 * }
	 * @params {*} ..args - аргументы, для передачи в метод
	 *
	 * @event - beforeChange.call($jQElem#get, event, curStepArray, nextStepArray, targetSelector) - перед переключением таба
	 * @event - afterChange.call($jQElem#get, event, prevStepArray, curStepArray, targetSelector) - после переключения таба
	 * @event - beforeScan.call($jQElem#get, event, menuItem, targetSelector) - перед поиском цели таба
	 *
	 * @return {jQuery} - $jQElem
	 */

	!IN4_BASE.addPlugin("jsTabs",
		{
			defaultOptions : {
				menuSelector      : "li > a",
				menuAttribute     : "href",
				blockContent      : document,
				menuClassActive   : "active",
				blockClassActive  : "active",
				blockClassVisible : "visible",
				preventDef        : true,
				event             : "click",
				notFirstActive    : false
			}
		},
		function(_ops){

			var TRIGGER_BEFORE_SCAN   = "beforeScan",
			    TRIGGER_BEFORE_CHANGE = "beforeChange",
			    TRIGGER_AFTER_CHANGE  = "afterChange",
			    TRIGGER_PREFIX        = "jsTabs",
			    ATTRIBUTE_PREFIX      = "data-tabs-",
			    ATTRIBUTE_ACTIVE      = "active"
				;

			var ERROR_NOT_PLUGIN = "It was not applied to this element plugin!",
			    ERROR_NOT_METHOD = "There is no such method / property!",
			    ERROR_NOT_OBJECT = "invalid argument for jsTabs!",
			    ERROR_NOT_ITEM   = "Incorrect item switch",
			    ERROR_YEP_PLUGIN = "This element is already applied plugin"
				;

			function _trigger(){

				/**
				 * _trigger()
				 *
				 * @return {Undefined}
				 */

				$(window).trigger('resize').trigger("scroll");
			}

			function _JSTabsConstructor(container, ops){

				/**
				 *
				 * _JSTabsConstructor(container, ops);
				 *
				 * @constructor {_JSTabsConstructor}
				 *
				 * @param {Element} container
				 * @param {Object} ops
				 *
				 */

				this.element = container;
				this.options = ops;
				this.data    = {
					curMenu      : null,
					prevMenu     : null,
					curBlock     : null,
					prevBlock    : null,
					curSelector  : null,
					prevSelector : null
				}

			}

			_JSTabsConstructor.ID = ("jsTabs" + Math.floor(Math.random() * 10e12));

			_JSTabsConstructor.getData = function(target){

				/**
				 * getData(target)
				 *
				 * @param {Element} target
				 *
				 * @return {Undefined|_JSTabsConstructor}
				 */

				return $(target).data(_JSTabsConstructor.ID);
			};

			_JSTabsConstructor.execute = function(target, name, args){

				/**
				 * execute(target, name, args)
				 *
				 * @param {Element} target
				 * @param {String} name
				 * @param {Array} args
				 *
				 * @return {Undefined}
				 */

				var data = _JSTabsConstructor.getData(target);

				if(typeof data == "undefined"){

					console.log(ERROR_NOT_PLUGIN, target);

				}else{

					return _JSTabsConstructor.prototype[name].apply(data, args);

				}

			};

			_JSTabsConstructor.handler = function(e){

				/**
				 *
				 * handler.call(element, e);
				 *
				 * @param {Element} element
				 * @param {Event} e
				 *
				 * @return {Undefined}
				 */

				var JSTabsController = _JSTabsConstructor.getData(e.delegateTarget),
				    innerOptions = JSTabsController.options;

				if(innerOptions.preventDef){
					e.preventDefault();
				}

				JSTabsController.toggleBy(this);

			};

			_JSTabsConstructor.create = function(target, options){

				/**
				 *
				 * create(target, options)
				 *
				 * @param {Element} target
				 * @param {Object} options
				 *
				 * @return {_JSTabsConstructor}
				 */

				if(_JSTabsConstructor.getData(target)){
					return console.log(ERROR_YEP_PLUGIN);
				}

				var innerOptions = $.extend({}, _ops.defaultOptions, options);

				////////////////////////////////////////////////////////////////////////
				var $menuContainer = $(target), $tabsUnits, $switchUnits = $();
				var JSTabsController                                     = new _JSTabsConstructor(target, innerOptions);
				////////////////////////////////////////////////////////////////////////

				var activeAttribute                                                                                                                                                              = target.getAttribute(ATTRIBUTE_PREFIX + ATTRIBUTE_ACTIVE), activeSelector                                                                                   = ("[" + innerOptions.menuAttribute + "='" + activeAttribute + "']"), activeClass = ("." + innerOptions.menuClassActive);

				//Обработчик на контейнер
				$menuContainer
					.data(_JSTabsConstructor.ID, JSTabsController)
					.on(innerOptions.event, innerOptions.menuSelector, _JSTabsConstructor.handler)
					.on(TRIGGER_AFTER_CHANGE + "." + TRIGGER_PREFIX, _trigger);

				//Активируем таб
				if(!innerOptions.notFirstActive){

					$tabsUnits = $menuContainer.find(innerOptions.menuSelector);

					// $switchUnits = $switchUnits
					// 	.add( $tabsUnits.filter(activeSelector) )
					// 	.add( $tabsUnits.filter(activeClass) )
					// 	.add( $tabsUnits.eq(0) )
					// ;
					//$switchUnits.get(0)

					JSTabsController.toggleBy($tabsUnits.filter(activeSelector).get(0) || $tabsUnits.filter(activeClass).get(0) || $tabsUnits.get(0));

				}

				return JSTabsController;

			};

			_JSTabsConstructor.init = function(options){

				/**
				 *
				 * init([options, value]);
				 *
				 * @param {Object} options
				 *
				 * @return {*}
				 *
				 */

				var __methods  = _JSTabsConstructor,
				    __prototype = _JSTabsConstructor.prototype
					;

				var args = IN4_BASE.sliceArguments.apply(1, arguments);

				if(typeof options == "string"){

					if(options in __prototype && typeof __prototype[options] == "function"){

						if(options.substr(0, 3) === "get"){

							return __methods.execute(this.get(0), options, args);

						}else{

							return this.each(function(){
								__methods.execute(this, options, args);
							});

						}

					}else{

						throw new Error(ERROR_NOT_METHOD);

					}

				}else if(options instanceof Object){

					return this.each(function(){
						__methods.create(this, options);
					});

				}else{

					throw new Error(ERROR_NOT_OBJECT);

				}

			};

			_JSTabsConstructor.prototype = {

				constructor : _JSTabsConstructor,

				toggleBy : function(target){

					/**
					 *
					 * toggleBy(target)
					 *
					 * @param {Element} target
					 *
					 * @return {Undefined}
					 */

					var options                                                                                                                                                                                        = this.options, data = this.data, element = this.element, trigger = (function($e){
						return function(trigName, trigData){
							$e.trigger(trigName + "." + TRIGGER_PREFIX, trigData.slice());
						}
					})($(element)), $newMenuTarget, $prevMenuTarget, prevMenuTarget, newMenuTarget, $newBlockTarget, $prevBlockTarget, newBlockTarget, prevBlockTarget, newBlockSelector, newBlockContext, triggerData = [];

					newMenuTarget    = target;
					$newMenuTarget   = $(newMenuTarget);
					newMenuTarget    = $newMenuTarget.get(0);
					newBlockSelector = newMenuTarget.getAttribute(options.menuAttribute);

					//Событие "Перед поиском элементов"
					trigger(TRIGGER_BEFORE_SCAN, [newMenuTarget, newBlockSelector]);

					newBlockContext = typeof options.blockContext == "function" ? options.blockContext(newMenuTarget, newBlockSelector) : options.blockContext;
					$newBlockTarget = $(newBlockSelector, newBlockContext).eq(0);

					newBlockTarget   = $newBlockTarget.get(0);
					prevBlockTarget  = data.curBlock;
					prevMenuTarget   = data.curMenu;
					$prevBlockTarget = $(prevBlockTarget);
					$prevMenuTarget  = $(prevMenuTarget);

					triggerData.push([prevMenuTarget, prevBlockTarget], [newMenuTarget, newBlockTarget], newBlockSelector);

					//Событие "До переключения"
					trigger(TRIGGER_BEFORE_CHANGE, triggerData);

					//Скрываем старый таб
					$prevMenuTarget.removeClass(options.menuClassActive);
					$prevBlockTarget.removeClass(options.blockClassActive + " " + (options.blockClassVisible || ""));

					//Показываем новый таб
					$newBlockTarget.addClass(options.blockClassActive);
					$newMenuTarget.addClass(options.menuClassActive);

					if(options.blockClassVisible){

						setTimeout(function(){
							$newBlockTarget.hasClass(options.blockClassActive) && $newBlockTarget.addClass(options.blockClassVisible);
						}, 100);

					}

					//Событие "После переключения"
					trigger(TRIGGER_AFTER_CHANGE, triggerData);

					data.prevBlock    = data.curBlock;
					data.prevMenu     = data.curMenu;
					data.prevSelector = data.curSelector;
					data.curMenu      = newMenuTarget;
					data.curBlock     = newBlockTarget;
					data.curSelector  = newBlockSelector;

				},

				toggleTo : function(n){

					/**
					 * toggleTo(n)
					 *
					 * @param {Number} n
					 *
					 * @return {Undefined}
					 */

					var e = $(this.options.menuSelector, this.container).get(n);

					if(e){

						this.toggleBy(e);

					}else{

						throw new Error(ERROR_NOT_ITEM);

					}

				},

				getCurrent : function(){

					/**
					 *
					 * getCurrent()
					 *
					 * @return {Array}
					 *
					 */

					var data = this.data;

					return [data.curMenu, data.curBlock, data.curSelector];

				},

				getPrevious : function(){

					/**
					 *
					 * getPrevious()
					 *
					 * @return {Array}
					 *
					 */

					var data = this.data;

					return [data.prevMenu, data.prevBlock, data.prevSelector];

				},

				getOptions : function(name){

					/**
					 *
					 * getOptions([,name])
					 *
					 * @param {String} name
					 *
					 * @return {*}
					 *
					 */

					var options = this.options;

					return typeof name == "string" ? options[name] : $.extend({}, options);

				},

				setOptions : function(name, value){

					/**
					 * setOptions(name, value);
					 *
					 * @param {String} name
					 * @param {*} value
					 *
					 * @return {Undefined}
					 */

					var options = this.options;

					if(typeof name == "string"){

						options[name] = value;

					}else if(name instanceof Object){

						$.extend(options, name);

					}else{

						throw new Error(ERROR_NOT_OBJECT);

					}

				},

				destroy : function(userHandlers){

					/**
					 * destroy([userHandlers])
					 *
					 * @param {Boolean} userHandlers
					 *
					 * @return {Undefined}
					 *
					 */

					var $element = $(this.element);

					$element
						.off(this.options.event, _JSTabsConstructor.handler)
						.removeData(_JSTabsConstructor.ID);

					if(userHandlers){

						$element
							.off(TRIGGER_AFTER_CHANGE + "." + TRIGGER_PREFIX)
							.off(TRIGGER_BEFORE_CHANGE + "." + TRIGGER_PREFIX)
							.off(TRIGGER_BEFORE_SCAN + "." + TRIGGER_PREFIX);

					}else{

						$element.off(TRIGGER_AFTER_CHANGE + "." + TRIGGER_PREFIX, _trigger);

					}

				},

				rebuild : function(options){

					/**
					 * rebuild([options])
					 *
					 * @param {Object} options
					 *
					 * @return {Undefined}
					 */

					this.destroy(false);

					_JSTabsConstructor.init.call($(this.element), options);

				}

			};


			$.jsTabs = {
				handlers : {
					AFTER_CHANGE  : TRIGGER_AFTER_CHANGE + "." + TRIGGER_PREFIX,
					BEFORE_CHANGE : TRIGGER_BEFORE_CHANGE + "." + TRIGGER_PREFIX,
					BEFORE_SCAN   : TRIGGER_BEFORE_SCAN + "." + TRIGGER_PREFIX
				}
			};

			////////////////////////////////////////
			return function(){
				return _JSTabsConstructor.init.apply(this, arguments);
			};
			////////////////////////////////////////

		}

	);

	/**
	 * $jQElem.jsYandexMaps([options])
	 *
	 * Yandex-карты
	 *
	 * @param {Object} options - настройки плагина {
	 *      @prop {Array} cords - массив с точками для карты, вида [{
	 *          @prop {Number} x,
	 *          @prop {Number} y,
	 *          @prop {String} address,
	 *          @prop {String} description,
	 *          @prop {String} title,
	 *          @prop {Object} icon {
	 *              @prop {Array} size,
	 *              @prop {Array} offset,
	 *              @prop {String} layout,
	 *              @prop {String} path
	 *          }
	 *      }...],
	 *      @prop {Array} center - координаты центра карты,
	 *      @prop {Number} zoom - масштаб карты
	 * }
	 *
	 * @return {jQuery} - $jQElem
	 */

	!IN4_BASE.addPlugin("jsYandexMaps",
		{
			defaultOptions : {
				main : {
					cords  : [],
					center : [0, 0],
					zoom   : 5
				},
				points : {
					address     : "",
					description : "",
					title       : "",
					icon : {
						size   : [0, 0],
						offset : [0, 0],
						layout : 'default#image',
						path   : ''
					}
				}
			}
		},
		function(_ops){

			var DATA_SAVE_CONTAINER = "yaMap";

			function GoMap(map, cords, center, zoom){

				/**
				 * new GoMap(map, cords, center, zoom)
				 *
				 * @constructor {GoMap}
				 *
				 * @param {Element} map
				 * @param {Array} cords
				 * @param {Array} center
				 * @param {Number} zoom
				 *
				 * @return this
				 */

				var defaultPointSettings = _ops.defaultOptions.points;

				var mapObject = new ymaps.Map(map, {
					center : center,
					zoom : zoom
				});

				cords.forEach(function(cordBox){

					var iconSettings = $.extend({}, _ops.defaultOptions.points.icon, ("icon" in cordBox) && cordBox.icon);

					mapObject.geoObjects.add(new ymaps.Placemark([cordBox.x, cordBox.y],
						{
							hintContent          : cordBox.title       || defaultPointSettings.title,
							balloonContent       : cordBox.description || defaultPointSettings.description,
							balloonContentHeader : cordBox.address     || defaultPointSettings.address
						},
						{
							iconLayout      : iconSettings.layout,
							iconImageHref   : iconSettings.path,
							iconImageSize   : iconSettings.size,
							iconImageOffset : iconSettings.offset
						}
					));

				});

				////////////////////////
				this.mapObject = mapObject;
				this.zoom = zoom;
				this.points = cords;
				////////////////////////
			}

			GoMap.prototype = {

				constructor : GoMap,

				toCords : function(x, y){

					/**
					 * toCords(x, y)
					 *
					 * @param {Number} x
					 * @param {Number} y
					 *
					 * @return {Undefined}
					 */

					var zoom = this.zoom, map = this.mapObject;

					map.panTo([x, y]).then(function () {
						map.setZoom(zoom);
					});
				},

				toSavedCords : function(n){

					/**
					 * toSavedCords(n)
					 *
					 * @param {Number} n
					 *
					 * @return {Undefined}
					 */

					var cords;

					if(n in this.points){

						cords = this.points[n];

						this.toCords(cords.x, cords.y);

					}else{
						throw "Выборки координат под номером" + " " + n + " " + "не существует";
					}

				}

			};

			return function(options){

				var $this = this, innerOptions = $.extend({}, _ops.defaultOptions.main, options);

				ymaps.ready(function(){

					$this.each(function(){

						$(this).data(DATA_SAVE_CONTAINER, new GoMap(
							this,
							innerOptions.cords,
							innerOptions.center,
							innerOptions.zoom
						));

					});

				});

				return $this;

			}

		}
	);

//Пока что не используется

	!IN4_BASE.addPlugin("jsAutoSize",
		{},
		function(){

			function AutoResizeOptions(element, axis, cf){

				/**
				 * AutoResizeOptions(element, axis, cf);
				 *
				 * @constructor {AutoResizeOptions}
				 *
				 * @param {Element} element
				 * @param {String} axis
				 * @param {Number} cf
				 *
				 */

				this.cf      = cf;
				this.element = element;
				this.axis    = axis;

			}

			AutoResizeOptions.prototype = {

				constructor : AutoResizeOptions,

				resize : function(){

					/**
					 * resize()
					 *
					 * @return {Undefined}
					 */

					var $element = $(this.element);

					if(this.axis == "X"){

						$element.width( $element.height() * data.cf );

					}else{

						$element.height( $element.width() / data.cf );

					}

				}

			};

			var sizeDump = [], $window = $(window);

			$window.on("resize", function(){

				function _resize(data){

					/**
					 * _resize(data)
					 *
					 * @param {AutoResizeOptions} data
					 *
					 * @return {Undefined}
					 */

					data.resize();

				}

				sizeDump.forEach(_resize);

			});

			return function(pW, pH, orientation){

				/**
				 * function(pW, pH, orientation);
				 *
				 * @param {Number} pW
				 * @param {Number} pH
				 * @param {String} orientation
				 *
				 * @return {jQuery}
				 */

				function _init(){

					/**
					 * _init()
					 *
					 * @return {Undefined} 
					 */

					sizeDump.push(
						new AutoResizeOptions(this, orientation, pW / pH)
					);

				}

				return this.each(_init), _onResize(), this;

			};

		}
	);
	return IN4_BASE;

}(jQuery, IN4_BASE);



