<!DOCTYPE html>
<html lang="ru">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>IN4_LIBRARY</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<form action="picker.php" method="get" name="main">

	<?php require "variables.php"; ?>

	<table cellpadding="10" border>
		<thead>
			<tr>
				<td>Вкл</td>
				<td>Название</td>
				<td>Описание</td>
				<td>Версия</td>
			</tr>
		</thead>
		<tbody>

			<?php

				foreach( scandir(PLUGINS_DIR) as $dir ){

			?>

					<tr>
						
			<?php

					$dirPath      = PLUGINS_DIR . "/" . $dir . "/";
					$infoFilePath = $dirPath . JS_MANIFEST_FILE;

					if(file_exists($dirPath) && file_exists($infoFilePath)){
						
						$manifest = json_decode( file_get_contents($infoFilePath), true );

						?>
						
							<td><input type="checkbox" autocomplete="off" name="<?=$dir;?>"></td>
							<td><?=$manifest['name'];?></td>
							<td><?=$manifest['description'];?></td>
							<td><?=$manifest['version'];?></td>

						<?php

					}
						
			?>
					</tr>
			<?php

				}

			?>

		</tbody>

	</table>


	<input type="submit" value="Собрать">

</form>

<script type="text/javascript" src="components/engine/engine.min.js"></script>
<script type="text/javascript">
	document.forms.main.onsubmit = function(e){

		IN4_BASE.Event.preventDef(e);

		var resultArray = Array.prototype.slice.call(this.elements)
			.filter(function(e){
				return e.checked;
			})
			.map(function(e){
				return e.name;
			})
		;

		location.href = this.action + "?plugins=" + resultArray.join(",");

	}
</script>

</body>
</html>