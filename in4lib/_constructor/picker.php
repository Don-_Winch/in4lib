<?php
	require "variables.php";
	header('Content-Type: text/javascript;charset=UTF-8');
?>

<?php

	$pluginsList = explode(",", urldecode($_GET['plugins']));
	$pluginsList = array_filter($pluginsList, function($pluginName){

		$dirPath = PLUGINS_DIR . $pluginName;
		$filePath = ($dirPath . "/" . JS_MANIFEST_FILE);

		return file_exists($dirPath) && file_exists($filePath);

	});

	$pluginInfo = array_map(function($e){

		$manifest = json_decode(
			file_get_contents(PLUGINS_DIR . $e . "/" . JS_MANIFEST_FILE), true
		);

		return ($e . " " . "v" . $manifest['version']);

	}, $pluginsList);
	$jsFileName = isset($_GET['minific']) ? JS_ENGINE_FILE_MIN : JS_ENGINE_FILE_NORMAL;

?>

/*
 *  IN4_LIBRARY
 *  Plugins : <?=implode(", ", $pluginInfo) . "\n";?>
 *  Date : <?=date("d.m.y H:i:s") . "\n";?>
*/

<?php

	echo "\n";
	echo file_get_contents(ENGINE_DIR . $jsFileName);

?>

!function($, IN4_BASE){

	<?php

		foreach($pluginsList as $k => $pluginName){

			echo "\n" . file_get_contents(PLUGINS_DIR . $pluginName . "/" . $jsFileName) . "\n";

		}

	?>
	return IN4_BASE;

}(jQuery, IN4_BASE);



