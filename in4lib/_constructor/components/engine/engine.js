"use strict";

var IN4_BASE = (function(AP, is){

	var in4closest = {

		Event : {
			
			preventDef       : function(e){

				/**
				 * preventDef(ev)
				 *
				 * @param {Event} ev
				 * @return {Undefined}
				 */

				var ev = e || event;
				("preventDefault" in ev) ? ev.preventDefault() : (ev.returnValue = false);
			},
			
			stopBuble        : function(e){

				/**
				 * preventDef(ev)
				 *
				 * @param {Event} ev
				 * @return {Undefined}
				 */

				var ev = e || event;
				("stopPropagation" in ev) ? ev.stopPropagation() : (ev.cancelBubble = true);

			},
			
		},
		
		String : {

			replaceAll       : function(str, search, replace){

				/**
				 * replaceAll(str, search, replace)
				 *
				 * Заменяет все вхождения search внутри str на replace
				 * Возвращает результат
				 *
				 *
				 * @param {String} str - целевая строка
				 * @param {String} search - что заменять
				 * @param {String} replace - чем заменять
				 * @return {String} - результирующая строка
				 */

				var lastIndex = -1, length = search.length, result = str;

				while( ~(lastIndex = str.indexOf(search, lastIndex + 1)) ){

					result = result.replace(
						search, typeof replace == "function" ? replace(lastIndex, search) : replace
					);

				}

				return result;

			},

			replaceAllByList : function(str, obj){

				var result = str, search, replace;

				for(search in obj){

					replace = obj[search];

					result = in4closest.String.replaceAll(result, search, replace);

				}

				return result;

			},
			
			replaceUseCache : function(str, searches){
				
			},

			strRev           : function(str){

				/**
				 * strRev(str)
				 *
				 * Возвращает перевернутую строку
				 *
				 * @param {String} str - целевая строка
				 * @return {String} - результирующая строка
				 */

				var sourceString = String(str),
				    resultString = "",
				    resultLength = sourceString.length
					;

				while(resultLength--){
					resultString += sourceString.charAt(resultLength);
				}

				return resultString;

			}
			
		},
		
		Number : {
			
			nToString        : function(n){

				/**
				 * nToString(n)
				 *
				 * @param {Number} n
				 *
				 * @return {String}
				 */

				return String(n < 10 ? ("0" + n) : n);

			},

			range            : function(min, max, number){

				/**
				 * range(min, max, number)
				 *
				 * Обрезает число, если оно не поподает в диапозон min <-> max
				 *
				 * @param {Number} min - минимальное значение диапозона
				 * @param {Number} max - максимальное значение диапозона
				 * @param {Number} number - целевое число
				 * @return {Number} - результирующее число
				 */

				return Math.max(min, Math.min(number, max));
			},
			
			isNaN            : function(n){
				
				return typeof n == "number" && !(n <= Infinity);
				
			}
			
		},

		Cookies : {

			getReplacedName : function(name){
				return name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1');
			},

			set : function(name, value, ops){

				/**
				 * set(name, value [,ops])
				 *
				 * @param {String} name
				 * @param {String} value
				 * @param {Object} ops {
				 *      @prop {Number|Boolean} expires,
				 *      @prop {String} domain,
				 *      @prop {String} secure,
				 *      @prop {String|Boolean} path
				 * }
				 *
				 * @return {Undefined}
				 */

				//

				function CookieOptions(){

					/**
					 * new CookieOptions()
					 *
					 * @constructor {CookieOptions}
					 */

					this.data = [];

				}

				CookieOptions.prototype = {

					constructor : CookieOptions,

					set : function(name, value){

						/**
						 * set(name, value);
						 *
						 * @param {String} name
						 * @param {*} value
						 *
						 * @return {Undefined}
						 */

						var curTime;

						switch(name){

							case "path":

								this.push("path", value === true ? "/" : value);

								break;

							case "secure":

								this.push("secure", value);

								break;

							case "domain":

								this.push("domain", value);

								break;

							case "expires":

								curTime = new Date().getTime();
								curTime += (value === true ? 666e10 : value);

								this.push("expires", curTime);

								break;


						}


					},

					push : function(name, value){

						/**
						 * push(name, value)
						 *
						 * @param {String} name
						 * @param {String} value
						 *
						 * @return {Undefined}
						 */

						this.data.push(name + "=" + value);
					},

					build : function(){

						/**
						 * build();
						 *
						 * @return {String}
						 */

						return this.data.length ? (this.data.join("; ") + "; ") : "";
					}

				};

				//

				var cookieName = name,
				    cookieValue = encodeURIComponent(value),
				    cookieOptions = new CookieOptions()
				;

				var innerOptions = in4closest.instanceofBy(Object, ops) ? ops : {};

				var cPath    = ("path" in innerOptions)    && innerOptions.path,
					cExpires = ("expires" in innerOptions) && innerOptions.expires,
				    cDomain  = ("domain" in innerOptions)  && innerOptions.domain,
				    cSecure  = ("secure" in innerOptions)  && innerOptions.secure
				;

				cPath    && cookieOptions.set("path", cPath);
				cExpires && cookieOptions.set("expires", cExpires);
				cDomain  && cookieOptions.set("domain", cDomain);
				cSecure  && cookieOptions.set("secure", cSecure);

				document.cookie = (cookieName + "=" + cookieValue + "; ") + cookieOptions.build();

			},

			get : function(name){

				/**
				 * get(name);
				 *
				 * @param {String} name
				 *
				 * @return {Null|String}
				 */

				var matches = document.cookie.match(
					new RegExp(
						"(?:^|; )" + this.getReplacedName(name) + "=([^;]*)"
					)
				);

				return matches && decodeURIComponent(matches[1]);


			},

			has : function(name){

				var reg = new RegExp("(^|; )" + this.getReplacedName(name) + "=");

				return reg.test(document.cookie);

			}

		},
		
		FF               : function(func, context, args){

			/**
			 * FF(func, [,context, args])
			 *
			 * @param {Function} func
			 * @param {*} context
			 * @param {Array} args
			 *
			 * @return {Undefined}
			 */

			typeof func == "function" && func.apply(context, args || undefined);

		},

		evalFunction     : function(argumentsNames, code, context, args){

			/**
			 * evalFunction(argumentsNames, code [, context, arguments])
			 *
			 * Выполняет код code как функцию с аргументами argumentsNames,
			 * передавая ей контекст context и аргументы arguments.
			 * Возвращает результат выполнения.
			 *
			 * @param {String} argumentsNames - имена аргументов (будут доступны по ним изнутри функции), через запятую
			 * @param {String} code - выполняемый внутри функции код
			 * @param {*} context - контекст, передаваемый в функцию
			 * @param {Array} args - массив аргументов, передаваемых в функцию
			 * @return {*} - результат выполнения функции
			 */


			try{

				return new Function(argumentsNames, code).apply(context || window, args);

			}catch(e){

				throw new Error("Ошибка в выражении: " + code);

			}
		},

		instanceofBy     : function(constructor, target){

			/**
			 * instanceofBy(constructor, target)
			 *
			 * Более правильная проверка на принадлежность target к конструктору constructor
			 *
			 * @param {Function} constructor - конструктор
			 * @param {Object} target - проверяемый экземпляр
			 * @return {Boolean} - true, если target создан через constructor
			 */


			return constructor !== target && target instanceof constructor;
		},

		require          : function(src, options){

			/**
			 *
			 * require(src [,options])
			 *
			 * @param {String} src
			 * @param {Object} options {
			 *      @prop {String} method
			 *      @prop {Boolean} async
			 *      @prop {Boolean} defer
			 *      @prop {Boolean} unique
			 * }
			 *
			 */


			function requireSync(path, defer, async){

				/**
				 * requireSync(path, defer, async)
				 *
				 * @param {String} path
				 * @param {Boolean} defer
				 * @param {Boolean} async
				 *
				 * @return {Undefined}
				 */

				var attributes = ["", "type='text/javascript'"];

				attributes.push("src" + "=" + "'" + path + "'");

				defer && attributes.push("defer");
				async && attributes.push("async");
				
				document.write("<scr" + "ipt" + attributes.join(" ") + "></scr" + "ipt>");

			}

			function requireAsync(path){

				/**
				 * requireAsync(path)
				 *
				 * @param {String} path
				 *
				 * @return {Undefined}
				 */

				var script = document.createElement("script");

				script.src   = path;
				script.type  = "text/javascript";

				document.getElementsByTagName("head")[0].appendChild(script);

			}

			function findScript(path){

				/**
				 * findScript(path)
				 *
				 * @param {String} path
				 *
				 * @return {Boolean}
				 */

				var scripts = document.getElementsByTagName("script");

				return Array.prototype.slice.call(scripts).some(function(script){
					return script.src == path;
				});

			}

			var innerOptions = $.extend(
				{
					method : "append",
					async  : false,
					defer  : false,
					unique : true
				},
				options
			);

			if(!innerOptions.unique || !findScript(src)){

				innerOptions.method == "append"
					? requireAsync(src)
					: requireSync(src, innerOptions.defer, innerOptions.async)
				;

			}



		},

		requestAnimation : function(fnc){

			/**
			 * requestAnimation(fnc)
			 *
			 * requestAnimationFrame для старых браузеров
			 *
			 * @param {Function} fnc - целевая функция
			 * @return {Undefined}
			 */

			is["animation"] ? requestAnimationFrame(fnc) : setTimeout(fnc, 1);

		},

		promise          : (function(){

			/**
			 *
			 * promise(func [,noStart]);
			 *
			 * @param {Function} func - ожидаемая функция. Запускается как func(Promise)
			 * @param {Boolean} noStart - true, если не нужно запускать обещание
			 *
			 * @return {Object} - обещание (Promise)
			 *
			 */

			var   STATUS_PENDING = "pending"
				, STATUS_REJECT  = "rejected"
				, STATUS_RESOLVE = "resolved"
				, STATUS_STARTED = "started"
				, STATUS_WAIT    = "waiting"
			;

			function f_add(t, f){

				/**
				 * f_add(t, f)
				 *
				 * @param {*} f
				 * @param {Array} t
				 *
				 * @return {Undefined}
				 */

				typeof f == "function" && t.push(f);
			}

			function _Promise(f){

				/**
				 *
				 * new _Promise(f [,noStart]);
				 *
				 * @constructor {_Promise}
				 *
				 * @param {Function} f
				 *
				 */

				this.watcher   = f;
				this.status    = STATUS_WAIT;
				this._handlers = {
					resolved  : [],
					rejected  : []
				};


			}

			_Promise.prototype = {
				constructor : _Promise,
				setDone : function(success){

					/**
					 *
					 * setDone(success);
					 *
					 * Устанавливает обработчик на переход в состояние resolved
					 *
					 * @param {Function} success
					 *
					 * @return {_Promise} - this
					 *
					 */

					return f_add(this._handlers.resolved, success), this;
				},
				setFail : function(failed){

					/**
					 *
					 * setFain(failed);
					 *
					 * Устанавливает обработчик на переход в состояние rejected
					 *
					 * @param {Function} failed
					 *
					 * @return {_Promise} - this
					 *
					 */

					return f_add(this._handlers.rejected, failed), this;
				},
				resolve : function(message){

					/**
					 *
					 * resolve(message);
					 *
					 * Переводит обещание в состояние "resolved".
					 * Вызывает обработчики, как func(Promise, message);
					 *
					 * @param {*} message
					 *
					 * @return {_Promise} - this
					 *
					 */

					var self = this;

					if(self.status == STATUS_PENDING){

						self.close(STATUS_RESOLVE);

						self._handlers.resolved.forEach(function(f){
							f(self, message);
						});

					}

					return this;

				},
				reject : function(error){

					/**
					 *
					 * reject(error);
					 *
					 * Переводит обещание в состояние "rejected".
					 * Вызывает обработчики, как func(Promise, error);
					 *
					 * @param {*} error
					 *
					 * @return {_Promise} - this
					 *
					 */

					var self = this;

					if(self.status == STATUS_PENDING){

						self.close(STATUS_REJECT);

						self._handlers.rejected.forEach(function(f){
							f(self, error);
						});

					}

					return this;

				},
				close  : function(type){

					/**
					 *
					 * close(type);
					 *
					 * Переводит обещание в состояние type, не вызывая никаких обработчиков.
					 *
					 * @param {String} type
					 *
					 * @return {_Promise} - this
					 *
					 */

					this.status = type;

					return this;

				},
				removeHandlers : function(type, func){

					/**
					 *
					 * removeHandlers(type [,func]);
					 *
					 * Удаляет все или конкретный обработчик, соответствующий типу type
					 *
					 * @param {String} type - тип обработчика (к примеру, resolved/rejected)
					 * @param {Function} func - искомая функция-обработчик
					 *
					 * @return {_Promise} - this
					 *
					 */

					var targetArray, targetIndex, targetLength;

					if(type in this._handlers){
						targetArray  = this._handlers[type];
						targetIndex  = 0;
						targetLength = targetArray.length;
					}

					if(targetArray){

						if(typeof func == "function"){
							targetIndex  = targetArray.indexOf(func);
							targetLength = 1;
						}

						targetArray.splice(targetIndex, targetLength);

					}

					return this;

				},
				start : function(f){

					/**
					 *
					 * start(f);
					 *
					 * Запускает обещание
					 *
					 * @param {Function} f - ожидаемая функция
					 *
					 * @return {_Promise} - this
					 *
					 */

					this.status = STATUS_PENDING;
					this.watcher = f || this.watcher;

					try{
						this.watcher(this);
					}catch(e){
						this.reject(e);
					}

					return this;

				},
				restart : function(removeHandlers){

					/**
					 *
					 * start(f);
					 *
					 * Перезапускает обещание с возможным удалением обработчиков
					 *
					 * @param {Boolean} removeHandlers - true, если нужно удалить все обработчики
					 *
					 * @return {_Promise} - this
					 *
					 */

					this.close(STATUS_STARTED);

					if(removeHandlers){
						this.removeHandlers();
					}

					return this.start(this.watcher);
				}
			};

			//////////////////////////

			function _init(f){

				/**
				 * _init(f)
				 *
				 * @param {Function} f
				 *
				 * @return {_Promise}
				 */

				return new _Promise(f);
			}

			_init.when = function(arr){

				/**
				 *
				 * when([arr]);
				 *
				 * @param {Array} arr
				 *
				 * @return {_Promise}
				 *
				 */

				var args       = [],
				    argsLength = 0
				;

				args = arr.filter(function(prom){
					return in4closest.instanceofBy(_Promise, prom);
				});
				
				return new _Promise(function(self){
					
					if(argsLength = args.length){

						args.forEach(function(promise){

							promise
								.setDone(function(){
									--argsLength || self.resolve();
								})
								.setFail(function(_){
									argsLength = 0; 
									self.reject(_);
								})
								.start()
							;

						});

					}else{

						self.resolve(self);

					}
					
				});

			};
			
			return _init;

		})(),

		sliceArguments   : function(){

			/**
			 * sliceArguments.apply(start [, arguments])
			 *
			 * @param {Arguments} arguments - псевдомассив аргументов
			 * @param {Number} start - позиция псевдомассива, с которой начинается сборка
			 *
			 * @return {Array} - результирующий массив
			 */
			
			return AP.slice.call(arguments, this);
		},

		stopwatch        : {

			/**
			 * stopwatch
			 *
			 * Обертка для console.time, console.timeEnd
			 *
			 */

			data : {},

			start : function(name){

				/**
				 * stopwatch.start(name)
				 *
				 * Аналог console.time(name)
				 *
				 * @param {String} name - идентификатор таймера
				 * @return {Undefined}
				 */

				var date, collection = this.data;

				if(is["consoleTime"]){

					console.time(name);

				}else if(is["consoleLog"]){

					date = (new Date()).getTime();
					
					console.log("Таймер " + name + " запущен");
					collection[name] = date;

				}

			},
			end : function(name){

				/**
				 * stopwatch.end(name)
				 *
				 * Аналог console.timeEnd(name)
				 *
				 * @param {String} name - идентификатор таймера
				 * @return {Undefined}
				 */

				var date, collection = this.data;

				if(is["consoleTime"]){

					console.timeEnd(name);

				}else if(is["consoleLog"] && name in collection){

					date = (new Date()).getTime() - collection[name];

					console.log("Отчет таймера " + name + ": " + date + "мс");
					
					delete collection[name];

				}

			}
		},

		addPlugin        : function (pluginName, options, initializer){

			/**
			 * addPlugin(pluginName, options, initializer)
			 *
			 * "Моторчик" для создания плагинов jQuery.
			 * jQuery.fn[pluginName] = initializer(options);
			 * jQuery.fn[pluginName][optionName] = options[optionName] (если optionName не начинается с "_");
			 *
			 * @param {String} pluginName
			 * @param {Object} options
			 * @param {Function} initializer
			 *
			 * @return {Boolean} - true, если плагин был добавлен
			 */


			if(typeof jQuery == "undefined"){
				return false;
			}

			//

			var jQueryConstructor = initializer(options), optionName, jQueryWrapper = function(){
				return jQueryConstructor.apply(this, arguments);
			};

			for(optionName in options){

				if( optionName.charAt(0) != "_" ){
					jQueryWrapper[optionName] = options[optionName];
				}

			}

			jQuery.fn[pluginName] = jQueryWrapper;

			return true;


		},

		buildPlugins     : (function(_plugList){

			/**
			 * buildPlugins(context)
			 *
			 * Инициализирует все плагины из коллекции
			 * &.add - добавление плагина
			 * &.drop - удаление плагина
			 * &.use - инициализация плагина
			 * &.is - проверка на существование плагина
			 * &.info - информация о плагине
			 * &.list - список плагинов
			 * &.get - получение объекта плагина
			 * &.newHandler, &.removeHandler - установка и удаление обработчиков
			 *
			 * @param {*} context - контекст, передаваемый в функцию-сборщик
			 * @return {Undefined}
			 */


			var   ERROR_NOT_FOUND = "Данный плагин не обнаружен"
				, ERROR_NOT_DESC  = "Описание к данному плагину отсутствует"
				, ERROR_YEP_FOUND = "Такой плагин уже существует!"
				, ERROR_NOT_CONSTRUCTOR = "У вашего плагина нет движка! (Метода constructor)"
			;

			//////////////////////

			function PluginUnit(name, description, constructor){

				/**
				 * new PluginUnit(name, description, constructor)
				 * 
				 * @constructor {PluginUnit}
				 *
				 * @param {String} name
				 * @param {String} description
				 * @param {Function} constructor
				 *
				 * @return {Undefined}
				 */
				
				this.name = name;
				this.picker = constructor;
				this.description = description;
				this.handlers = {};
			}

			PluginUnit.prototype = {
				constructor: PluginUnit,
				execute : function(context){

					/**
					 * execute([context])
					 *
					 * Вызывает сборщик и обработчики "after", "before"
					 * Передает context них, в качетсве обычного аргумента
					 *
					 * @param {*} context
					 *
					 * @return {Undefined}
					 */

					var data = [context];

					this.triggerHandler("before", data);

					this.picker.apply(this, data);

					this.triggerHandler("after", data);


				},
				setHandler : function(handlerName, handlerFunc){

					/**
					 * setHandler(handlerName, handlerFunc)
					 *
					 * Устанавливает обработчик
					 *
					 * @param {String} handlerName
					 * @param {Function} handlerFunc
					 *
					 * @return {Undefined}
					 */

					var handlers = this.handlers;

					if(!(handlerName in handlers)){
						handlers[handlerName] = [];
					}

					handlers[handlerName].push(handlerFunc);

				},
				removeHandler : function(handlerName, handlerFunc){

					/**
					 * removeHandler(handlerName, handlerFunc)
					 *
					 * Удаляет обработчик
					 *
					 * @param {String} handlerName
					 * @param {Function} handlerFunc
					 *
					 * @return {Undefined}
					 */

					if(!(handlerName in this.handlers)){
						return;
					}

					var handlerUnit = this.handlers[handlerName], i = handlerUnit.length;

					while(i--){

						if(handlerUnit[i] === handlerFunc){
							handlerUnit.splice(i++, 1);
						}

					}

				},
				triggerHandler : function(handlerName, args){

					/**
					 * triggerHandler(handlerName, args)
					 *
					 * Вызывает обработчики
					 *
					 * @param {String} handlerName
					 * @param {Array} args - аргументы, передаваемые в обработчик
					 *
					 * @return {Undefined}
					 */

					var self = this;

					if(handlerName in self.handlers){

						self.handlers[handlerName].forEach(function(f){
							f.apply(self, args);
						});

					}


				}
			};

			///////////////////////


			function _plugInit(context){

				/**
				 * _plugInit(context)
				 *
				 * @param {*} context
				 *
				 * @return {Undefined}
				 */

				_plugInit.use(context, _plugInit.list());

			}

			_plugInit.plugins = _plugList;
			_plugInit.is = function(plugName){

				/**
				 * buildPlugins.is(plugName)
				 *
				 * Проверяет существование плагина
				 *
				 * @param {String} plugName
				 *
				 * @return {Boolean}
				 */

				return (plugName in _plugList);
			};
			_plugInit.use = function(context, plugNames){

				/**
				 * buildPlugins.use(context, plugNames)
				 *
				 * Инициализирует плигны из plugNames
				 *
				 * @param {*} context - первый аргумент, передаваемый в функцию-сборщик
				 * @param {String|Array} plugNames - идентификатор или массив идентификаторов плагинов, которые нужно запустить
				 *
				 * @return {Undefined}
				 */


				function eachNames(name){

					if(_plugInit.is(name)){

						in4closest.requestAnimation(function(){
							_plugList[name].execute(context);
						});

					}

				}

				in4closest.instanceofBy(Array, plugNames) ? plugNames.forEach(eachNames) : eachNames(plugNames);

			};
			_plugInit.info = function(plugName){

				/**
				 * buildPlugins.info(plugName)
				 *
				 * Возвращает информацию о плагине
				 *
				 * @param {String} plugName - идентификатор целевого плагина
				 * @return {String} - информация о плагине
				 */


				if(_plugInit.is(plugName)){

					return _plugList[plugName].description || ERROR_NOT_DESC;

				}else{

					throw new Error(ERROR_NOT_FOUND);

				}

			};
			_plugInit.get = function(plugName){

				/**
				 * buildPlugins.get(plugName)
				 *
				 * Возваращает PluginUnit-объект
				 *
				 * @param {String} plugName - идентификатор целевого плагина
				 *
				 * @return {PluginUnit}
				 */

				return _plugInit.is(plugName) && _plugList[plugName];

			};
			_plugInit.add = function(plugName, plugBox){

				/**
				 * buildPlugins.add(plugName, plugBox)
				 *
				 * Добавляет плагин в коллекцию
				 *
				 * @param {String} plugName - идентификатор нового плагина
				 * @param {Object} plugBox  - тело плагина {
				 *      @prop {Function} constructor - функция-сборщик. Вызывается как constructor.call(PlugUnit, context, argument) (см. buildPlugins.use),
				 *      @prop {String} description - описание плагина
				 * }
				 *
				 * @return {PluginUnit}
				 */

				if(_plugInit.is(plugName)){

					throw new Error(ERROR_YEP_FOUND);

				}else if(typeof plugBox.constructor == "function"){

					_plugList[plugName] = new PluginUnit(plugName, plugBox.description, plugBox.constructor);

				}else{

					throw new Error(ERROR_NOT_CONSTRUCTOR);

				}

			};
			_plugInit.list = function(){

				/**
				 * buildPlugins.list()
				 *
				 * Возвращает список плагинов в коллекции
				 *
				 * @return {Array} - массив идентификаторов
				 */

				//without Object.keys

				var keyBox = [], key;

				for(key in _plugList){
					keyBox.push(key);
				}

				return keyBox;
			};
			_plugInit.drop = function(plugName){

				/**
				 * buildPlugins.drop(plugName)
				 *
				 * Удаляет плагин из коллекции
				 *
				 * @return {Boolean} - true, если не произошло ошибки
				 */

				return _plugInit.is(plugName) && (delete _plugList[plugName]);

			};
			_plugInit.newHandler = function(plugName, handlerName, handlerFunc){

				/**
				 * buildPlugins.newHandler(plugName, handlerName, handlerFunc)
				 *
				 * Установка обработчика
				 *
				 * @param plugName - идентификатор плагина
				 * @param handlerName - название обработчика
				 * @param handlerFunc - функция обработчика
				 *
				 * @return {Undefined}
				 */

				if(_plugInit.is(plugName)){

					_plugList[plugName].setHandler(handlerName, handlerFunc);

				}else{

					throw new Error(ERROR_NOT_FOUND);

				}

			};
			_plugInit.removeHandler = function(plugName, handlerName, handlerFunc){

				/**
				 * buildPlugins.removeHandler(plugName, handlerName, handlerFunc)
				 *
				 * Удаление обработчика
				 *
				 * @param plugName - идентификатор плагина
				 * @param handlerName - название обработчика
				 * @param handlerFunc - функция обработчика
				 *
				 * @return {Undefined}
				 */

				if(_plugInit.is(plugName)){

					_plugList[plugName].removeHandler(handlerName, handlerFunc);

				}else{

					throw new Error(ERROR_NOT_FOUND);

				}


			};

			return _plugInit;

		})({}),

		version : "3.1.0"

	};

	return in4closest;
	
})(
	Array.prototype, {
		"animation"   : ("requestAnimationFrame" in window),
		"consoleTime" : ("time" in console),
		"consoleLog"  : ("log" in console)
 	}
);

