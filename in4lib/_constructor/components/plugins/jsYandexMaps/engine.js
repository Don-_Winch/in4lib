/**
 * $jQElem.jsYandexMaps([options])
 *
 * Yandex-карты
 *
 * @param {Object} options - настройки плагина {
 *      @prop {Array} cords - массив с точками для карты, вида [{
 *          @prop {Number} x,
 *          @prop {Number} y,
 *          @prop {String} address,
 *          @prop {String} description,
 *          @prop {String} title,
 *          @prop {Object} icon {
 *              @prop {Array} size,
 *              @prop {Array} offset,
 *              @prop {String} layout,
 *              @prop {String} path
 *          }
 *      }...],
 *      @prop {Array} center - координаты центра карты,
 *      @prop {Number} zoom - масштаб карты
 * }
 *
 * @return {jQuery} - $jQElem
 */

!IN4_BASE.addPlugin("jsYandexMaps",
	{
		defaultOptions : {
			main : {
				cords  : [],
				center : [0, 0],
				zoom   : 5
			},
			points : {
				address     : "",
				description : "",
				title       : "",
				icon : {
					size   : [0, 0],
					offset : [0, 0],
					layout : 'default#image',
					path   : ''
				}
			}
		}
	},
	function(_ops){

		var DATA_SAVE_CONTAINER = "yaMap";

		function GoMap(map, cords, center, zoom){

			/**
			 * new GoMap(map, cords, center, zoom)
			 * 
			 * @constructor {GoMap}
			 *
			 * @param {Element} map
			 * @param {Array} cords
			 * @param {Array} center
			 * @param {Number} zoom
			 *
			 * @return this
			 */

			var defaultPointSettings = _ops.defaultOptions.points;

			var mapObject = new ymaps.Map(map, {
				center : center,
				zoom : zoom
			});

			cords.forEach(function(cordBox){

				var iconSettings = $.extend({}, _ops.defaultOptions.points.icon, ("icon" in cordBox) && cordBox.icon);
				
				mapObject.geoObjects.add(new ymaps.Placemark([cordBox.x, cordBox.y],
					{
						hintContent          : cordBox.title       || defaultPointSettings.title,
						balloonContent       : cordBox.description || defaultPointSettings.description,
						balloonContentHeader : cordBox.address     || defaultPointSettings.address
					},
					{
						iconLayout      : iconSettings.layout,
						iconImageHref   : iconSettings.path,
						iconImageSize   : iconSettings.size,
						iconImageOffset : iconSettings.offset
					}
				));

			});

			////////////////////////
			this.mapObject = mapObject;
			this.zoom = zoom;
			this.points = cords;
			////////////////////////
		}

		GoMap.prototype = {

			constructor : GoMap,

			toCords : function(x, y){
				
				/**
				 * toCords(x, y)
				 * 
				 * @param {Number} x
				 * @param {Number} y
				 * 
				 * @return {Undefined}
				 */

				var zoom = this.zoom, map = this.mapObject;

				map.panTo([x, y]).then(function () {
					map.setZoom(zoom);
				});
			},

			toSavedCords : function(n){

				/**
				 * toSavedCords(n)
				 *
				 * @param {Number} n
				 *
				 * @return {Undefined}
				 */

				var cords;

				if(n in this.points){
					
					cords = this.points[n];

					this.toCords(cords.x, cords.y);

				}else{
					throw "Выборки координат под номером" + " " + n + " " + "не существует";
				}

			}

		};

		return function(options){

			var $this = this, innerOptions = $.extend({}, _ops.defaultOptions.main, options);

			ymaps.ready(function(){

				$this.each(function(){

					$(this).data(DATA_SAVE_CONTAINER, new GoMap(
						this,
						innerOptions.cords,
						innerOptions.center,
						innerOptions.zoom
					));

				});

			});

			return $this;

		}

	}
);