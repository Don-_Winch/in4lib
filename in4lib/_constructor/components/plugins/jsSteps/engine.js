/**
 * $jQElem.jsSteps([options])
 *
 * Компонент "Steps" (Шаги)
 *
 * @param {Object} options - настройки плагина {
 *      @prop {String} targetSelector - селектор целевых дочерних элементов,
 *      @prop {String} color - цвет прогресс-бара,
 *      @prop {Number} startStep - стартовый шаг,
 *      @prop {Boolean} notTriggeredStart - если true, то при инициализации не будут вызваны обработчики beforeChange и afterChange,
 *      @prop {String} nextButton - селектор кнопки "вперед",
 *      @prop {String} prevButton - селектор кнопки "назад",
 *      @prop {Function} condition - функция, вызываемая при переходе к другому шагу, должна возвращать true, если разрешает переход.
 * }
 *
 * @event - beforeCheck.call($jQElem#get, controllerObject, curStepObject, nextStepObject, isEmulation)  - перед проверкой options.condition
 * @event - beforeChange.call($jQElem#get, controllerObject, curStepObject, nextStepObject, isEmulation) - перед переключением щага
 * @event - afterChange.call($jQElem#get, controllerObject, prevStepObject, curStepObject, isEmulation)  - после переключения щага
 * @return {jQuery} - $jQElem
 */

!IN4_BASE.addPlugin("jsSteps",
	{
		defaultOptions : {
			targetSelector    : "*",
			color             : "#000000",
			startStep         : 0,
			notTriggeredStart : false,
			nextButton : "a[href='#js-Steps_next']",
			prevButton : "a[href='#js-Steps_prev']"
		}
	},
	function(_ops){
		
		var   CLASS_BODY = "js-Steps_body"
			, CLASS_LINE = "js-Steps_line"
			, CLASS_LINE_HEADER = "js-Steps_lineHeader"
			, CLASS_LINE_CURRENT = "js-Steps_lineCurrent"
			, CLASS_LINE_PROGRESS = "js-Steps_lineProgress"
			, CLASS_LINE_TRIGGER = "js-Steps_lineTrigger"
			, TAG_BODY = "<div />"
			, TAG_LINE = "<div />"
			, TAG_LINE_HEADER = "<h2 />"
			, TAG_LINE_CURRENT = "<span />"
			, TAG_LINE_PROGRESS = "<div />"
			, TAG_LINE_TRIGGER = "<div />"
			, CLASS_STEP = "js-Steps_target"
			, CLASS_STEP_DISABLED = "disabled"
			, CLASS_STEP_ACTIVE  = "enabled"
			, TRIGGER_PREFIX = "jsSteps"
			, TRIGGER_BEFORE_CHANGE = "beforeChange"
			, TRIGGER_AFTER_CHANGE = "afterChange"
			, TRIGGER_BEFORE_CHECK = "beforeCheck"
			, DATA_SAVE_CONTAINER = "jsSteps"
		;

		function CreateStepLine(container, $steps, color, condition, callBack){

			/**
			 * new CreateStepLine(container, $steps, color, condition, callBack)
			 *
			 * @constructor
			 * @this {CreateStepLine}
			 * 
			 * @param {Element} container
			 * @param {jQuery} $steps
			 * @param {String} color
			 * @param {Function} condition
			 * @param {Function} callBack
			 */

			var $stepBody          = $(TAG_BODY).addClass(CLASS_BODY),
			    $stepTop           = $(TAG_LINE).addClass(CLASS_LINE),
			    $stepTop_Header    = $(TAG_LINE_HEADER).addClass(CLASS_LINE_HEADER),
			    $stepTop_Number    = $(TAG_LINE_CURRENT).addClass(CLASS_LINE_CURRENT),
			    $stepTop_Line      = $(TAG_LINE_PROGRESS).addClass(CLASS_LINE_PROGRESS),
			    $stepTop_Progress  = $(TAG_LINE_TRIGGER)
				    .addClass(CLASS_LINE_TRIGGER)
				    .css("background", color)
			;

			//// Все аппенды ///

			$stepTop_Line.append($stepTop_Progress);

			$stepTop_Header
				.append($stepTop_Number)
				.append("<span>шаг</span>")
			;

			$stepTop
				.append($stepTop_Header)
				.append($stepTop_Line)
			;

			$steps
				.addClass(CLASS_STEP + " " + CLASS_STEP_DISABLED)
				.appendTo($stepBody)
			;

			/////////////////////////////////////

			this.currentStep = null;
			this.stepCount   = $steps.length;
			this.callBack   = callBack;
			this.condition  = condition;
			this.containers  = {
				header : $stepTop.get(0),
				body   : $stepBody.get(0),
				main   : container
			};
			this.controllers = {
				number : $stepTop_Number.get(0),
				line   : $stepTop_Progress.get(0),
				steps  : $steps.get()
			};

			/////////////////////////////////////

		}

		CreateStepLine.prototype = {

			toStep : function(number, notTriggered, emulation, withoutCondition){

				/**
				 * toStep(number [,notTriggered, emulation, withoutCondition]);
				 *
				 * @param {Number} number - номер шага
				 * @param {Boolean} notTriggered - true, если не нужно вызывать обработчики
				 * @param {Boolean} emulation - true, если функция вызвана программно
				 * @param {Boolean} withoutCondition - true, если не требуется проверка CreateStepLine.condition
				 *
				 * @return {Undefined}
				 */

				var newStepNumber = IN4_BASE.Number.range(0, this.stepCount - 1, number),
				    oldStepNumber = this.currentStep,
				    newStepElement = this.controllers.steps[newStepNumber],
				    oldStepElement = this.controllers.steps[oldStepNumber],
				    newVisualStepNumber  = newStepNumber + 1,
				    newVisualStepWidth   = Math.min(100, Math.ceil(100 / this.stepCount) * newVisualStepNumber)
				;

				var triggerData = [
					this,
					{
						index   : oldStepNumber,
						element : oldStepElement
					},
					{
						index   : newStepNumber,
						element : newStepElement
					},
					emulation || false
				];

				var allowStep  =  withoutCondition ||
				    typeof this.condition == "function" &&
				    this.condition.apply(this.containers.main, triggerData)
				;

				notTriggered || this.callBack(TRIGGER_BEFORE_CHECK, triggerData);

				if(newStepNumber !== oldStepNumber && allowStep){

					notTriggered || this.callBack(TRIGGER_BEFORE_CHANGE, triggerData);

					$(newStepElement)
						.add(oldStepElement)
						.toggleClass(CLASS_STEP_ACTIVE + " " + CLASS_STEP_DISABLED)
					;

					$(this.controllers.number).text(newVisualStepNumber);
					$(this.controllers.line).css("width", newVisualStepWidth + "%");

					this.currentStep = newStepNumber;

					notTriggered || this.callBack(TRIGGER_AFTER_CHANGE, triggerData);

				}

				
			}

		};

		return function(options){
			
			var innerOptions = $.extend({}, _ops.defaultOptions, options);

			return this.each(function(){

				var handlerID = "jsSteps" + Math.floor(Math.random() * 2e10);

				var  self  = this,
				    $self  = $(this),
					$steps = $self.children(innerOptions.targetSelector)
				;
				
				var engine = new CreateStepLine(
					this, $steps, innerOptions.color, innerOptions.condition,
					function(name, data){
						$(self).trigger(name + "." + TRIGGER_PREFIX, data.slice());
					}
				);

				engine.toStep(innerOptions.startStep, innerOptions.notTriggeredStart, true, true);

				$self
					.on(handlerID, function(e, step){
						engine.toStep(step);
					})
					.on("click", innerOptions.nextButton, function(e){

						e.preventDefault();
						e.stopPropagation();

						$(this).trigger(handlerID, [engine.currentStep + 1]);

					})
					.on("click", innerOptions.prevButton, function(e){

						e.preventDefault();
						e.stopPropagation();

						$(this).trigger(handlerID, [engine.currentStep - 1]);

					})
					.data(DATA_SAVE_CONTAINER, engine)
					.prepend(engine.containers.body)
					.prepend(engine.containers.header)
				;

			});
			
		}

	}
);