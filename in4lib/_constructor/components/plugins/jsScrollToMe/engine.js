/**
 * $jQElem.jsScrollToMe([px, time, container])
 *
 * Прокруччивает container к верху элемента $jQElem минус px за время time
 *
 * @param {Number} px - требуемый отступ от верхней границы окна до блока
 * @param {Number} time - скорость выполнения прокрутки
 * @param {String|Element} container - селектор для поиска или ссылка на элемент, который будем прокручивать
 * @return {jQuery} - $jQElem
 */

!IN4_BASE.addPlugin("jsScrollToMe",
	{
		defaultOptions : {
			indent : 10,
			speed  : 500,
			targetSelector : "body, html"
		}
	},
	function(_ops){
		
		return function(px, time, selector){

			var offset = this.offset(), needOffset, target;

			if(offset){

				needOffset = offset.top - (px || _ops.defaultOptions.indent);
				target     = selector || _ops.defaultOptions.targetSelector;

				$(target).scrollTop() === needOffset || $(target).animate(
					{ scrollTop : needOffset },
					time || _ops.defaultOptions.speed
				);

			}

			return this;

		}

	}
);