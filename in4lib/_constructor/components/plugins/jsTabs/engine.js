/**
 * $jQElem.jsTabs([options, ..args])
 *
 * Компонент "Tabs" (Вкладки)
 *
 * @param {Object|String} options - имя метода (потомка _JSTabsConstructor) или настройки плагина {
 *      @prop {String} menuSelector - СSS-селектор переключателей,
 *      @prop {String} menuAttribute - HTML-атрибут переключателя, содержаший селектор цели,
 *      @prop {String|Function} blockContext - Элемент, в котором лежат целевые блоки (если функция, то вызывается перед переключением таба как blockContext(menuItem, targetSelector) и должна возвращать элемент или селектор),
 *      @prop {String} menuClassActive - Класс, устанавливаемый активному табу,
 *      @prop {String} menuAttribute - HTML-атрибут переключателя, содержаший селектор цели,
 *      @prop {String} blockClassActive - Класс, устанавливаемый активному целевому блоку,
 *      @prop {String} blockClassVisible - Класс, устанавливаемый активному целевому блоку спустя 100мс после активации,
 *      @prop {Boolean} preventDef - нужна ли отмена дефолтного действия браузера при переключении табов,
 *      @prop {Boolean} notFirstActive - если не нужно активировать 1 из табов при инициализации,
 *      @prop {String} event - событие для активации таба ("click", "mouseover" и т.п)
 * }
 * @params {*} ..args - аргументы, для передачи в метод
 *
 * @event - beforeChange.call($jQElem#get, event, curStepArray, nextStepArray, targetSelector) - перед переключением таба
 * @event - afterChange.call($jQElem#get, event, prevStepArray, curStepArray, targetSelector) - после переключения таба
 * @event - beforeScan.call($jQElem#get, event, menuItem, targetSelector) - перед поиском цели таба
 *
 * @return {jQuery} - $jQElem
 */

!IN4_BASE.addPlugin("jsTabs",
	{
		defaultOptions : {
			menuSelector      : "li > a",
			menuAttribute     : "href",
			blockContent      : document,
			menuClassActive   : "active",
			blockClassActive  : "active",
			blockClassVisible : "visible",
			preventDef        : true,
			event             : "click",
			notFirstActive    : false
		}
	},
	function(_ops){

		var TRIGGER_BEFORE_SCAN   = "beforeScan",
		    TRIGGER_BEFORE_CHANGE = "beforeChange",
		    TRIGGER_AFTER_CHANGE  = "afterChange",
		    TRIGGER_PREFIX        = "jsTabs",
		    ATTRIBUTE_PREFIX      = "data-tabs-",
		    ATTRIBUTE_ACTIVE      = "active"
		;

		var ERROR_NOT_PLUGIN = "It was not applied to this element plugin!",
		    ERROR_NOT_METHOD = "There is no such method / property!",
		    ERROR_NOT_OBJECT = "invalid argument for jsTabs!",
		    ERROR_NOT_ITEM   = "Incorrect item switch",
		    ERROR_YEP_PLUGIN = "This element is already applied plugin"
		;

		function _trigger(){

			/**
			 * _trigger()
			 * 
			 * @return {Undefined}
			 */

			$(window).trigger('resize').trigger("scroll");
		}

		function _JSTabsConstructor(container, ops){

			/**
			 *
			 * _JSTabsConstructor(container, ops);
			 *
			 * @constructor {_JSTabsConstructor}
			 *
			 * @param {Element} container
			 * @param {Object} ops
			 *
			 */

			this.element = container;
			this.options = ops;
			this.data    = {
				curMenu      : null,
				prevMenu     : null,
				curBlock     : null,
				prevBlock    : null,
				curSelector  : null,
				prevSelector : null
			}

		}

		_JSTabsConstructor.ID = ("jsTabs" + Math.floor(Math.random() * 10e12));

		_JSTabsConstructor.getData = function(target){

			/**
			 * getData(target)
			 *
			 * @param {Element} target
			 *
			 * @return {Undefined|_JSTabsConstructor}
			 */

			return $(target).data(_JSTabsConstructor.ID);
		};

		_JSTabsConstructor.execute = function(target, name, args){

			/**
			 * execute(target, name, args)
			 *
			 * @param {Element} target
			 * @param {String} name
			 * @param {Array} args
			 *
			 * @return {Undefined}
			 */

			var data = _JSTabsConstructor.getData(target);

			if(typeof data == "undefined"){

				console.log(ERROR_NOT_PLUGIN, target);

			}else{

				return _JSTabsConstructor.prototype[name].apply(data, args);

			}

		};

		_JSTabsConstructor.handler = function(e){

			/**
			 *
			 * handler.call(element, e);
			 *
			 * @param {Element} element
			 * @param {Event} e
			 *
			 * @return {Undefined}
			 */

			var JSTabsController = _JSTabsConstructor.getData(e.delegateTarget),
			    innerOptions = JSTabsController.options;

			if(innerOptions.preventDef){
				e.preventDefault();
			}

			JSTabsController.toggleBy(this);

		};

		_JSTabsConstructor.create = function(target, options){

			/**
			 *
			 * create(target, options)
			 *
			 * @param {Element} target
			 * @param {Object} options
			 *
			 * @return {_JSTabsConstructor}
			 */

			if(_JSTabsConstructor.getData(target)){
				return console.log(ERROR_YEP_PLUGIN);
			}

			var innerOptions = $.extend({}, _ops.defaultOptions, options);

			////////////////////////////////////////////////////////////////////////
			var $menuContainer = $(target), $tabsUnits, $switchUnits = $();
			var JSTabsController                                     = new _JSTabsConstructor(target, innerOptions);
			////////////////////////////////////////////////////////////////////////

			var activeAttribute                                                                                                                                                              = target.getAttribute(ATTRIBUTE_PREFIX + ATTRIBUTE_ACTIVE), activeSelector                                                                                   = ("[" + innerOptions.menuAttribute + "='" + activeAttribute + "']"), activeClass = ("." + innerOptions.menuClassActive);

			//Обработчик на контейнер
			$menuContainer
				.data(_JSTabsConstructor.ID, JSTabsController)
				.on(innerOptions.event, innerOptions.menuSelector, _JSTabsConstructor.handler)
				.on(TRIGGER_AFTER_CHANGE + "." + TRIGGER_PREFIX, _trigger);

			//Активируем таб
			if(!innerOptions.notFirstActive){

				$tabsUnits = $menuContainer.find(innerOptions.menuSelector);

				// $switchUnits = $switchUnits
				// 	.add( $tabsUnits.filter(activeSelector) )
				// 	.add( $tabsUnits.filter(activeClass) )
				// 	.add( $tabsUnits.eq(0) )
				// ;
				//$switchUnits.get(0)

				JSTabsController.toggleBy($tabsUnits.filter(activeSelector).get(0) || $tabsUnits.filter(activeClass).get(0) || $tabsUnits.get(0));

			}

			return JSTabsController;

		};

		_JSTabsConstructor.init = function(options){

			/**
			 *
			 * init([options, value]);
			 *
			 * @param {Object} options
			 *
			 * @return {*}
			 *
			 */

			var __methods  = _JSTabsConstructor,
			    __prototype = _JSTabsConstructor.prototype
			;

			var args = IN4_BASE.sliceArguments.apply(1, arguments);

			if(typeof options == "string"){

				if(options in __prototype && typeof __prototype[options] == "function"){

					if(options.substr(0, 3) === "get"){

						return __methods.execute(this.get(0), options, args);

					}else{

						return this.each(function(){
							__methods.execute(this, options, args);
						});

					}

				}else{

					throw new Error(ERROR_NOT_METHOD);

				}

			}else if(options instanceof Object){

				return this.each(function(){
					__methods.create(this, options);
				});

			}else{

				throw new Error(ERROR_NOT_OBJECT);

			}

		};

		_JSTabsConstructor.prototype = {

			constructor : _JSTabsConstructor,

			toggleBy : function(target){

				/**
				 *
				 * toggleBy(target)
				 *
				 * @param {Element} target
				 *
				 * @return {Undefined}
				 */

				var options                                                                                                                                                                                        = this.options, data = this.data, element = this.element, trigger = (function($e){
					return function(trigName, trigData){
						$e.trigger(trigName + "." + TRIGGER_PREFIX, trigData.slice());
					}
				})($(element)), $newMenuTarget, $prevMenuTarget, prevMenuTarget, newMenuTarget, $newBlockTarget, $prevBlockTarget, newBlockTarget, prevBlockTarget, newBlockSelector, newBlockContext, triggerData = [];

				newMenuTarget    = target;
				$newMenuTarget   = $(newMenuTarget);
				newMenuTarget    = $newMenuTarget.get(0);
				newBlockSelector = newMenuTarget.getAttribute(options.menuAttribute);

				//Событие "Перед поиском элементов"
				trigger(TRIGGER_BEFORE_SCAN, [newMenuTarget, newBlockSelector]);

				newBlockContext = typeof options.blockContext == "function" ? options.blockContext(newMenuTarget, newBlockSelector) : options.blockContext;
				$newBlockTarget = $(newBlockSelector, newBlockContext).eq(0);

				newBlockTarget   = $newBlockTarget.get(0);
				prevBlockTarget  = data.curBlock;
				prevMenuTarget   = data.curMenu;
				$prevBlockTarget = $(prevBlockTarget);
				$prevMenuTarget  = $(prevMenuTarget);

				triggerData.push([prevMenuTarget, prevBlockTarget], [newMenuTarget, newBlockTarget], newBlockSelector);

				//Событие "До переключения"
				trigger(TRIGGER_BEFORE_CHANGE, triggerData);

				//Скрываем старый таб
				$prevMenuTarget.removeClass(options.menuClassActive);
				$prevBlockTarget.removeClass(options.blockClassActive + " " + (options.blockClassVisible || ""));

				//Показываем новый таб
				$newBlockTarget.addClass(options.blockClassActive);
				$newMenuTarget.addClass(options.menuClassActive);

				if(options.blockClassVisible){

					setTimeout(function(){
						$newBlockTarget.hasClass(options.blockClassActive) && $newBlockTarget.addClass(options.blockClassVisible);
					}, 100);

				}

				//Событие "После переключения"
				trigger(TRIGGER_AFTER_CHANGE, triggerData);

				data.prevBlock    = data.curBlock;
				data.prevMenu     = data.curMenu;
				data.prevSelector = data.curSelector;
				data.curMenu      = newMenuTarget;
				data.curBlock     = newBlockTarget;
				data.curSelector  = newBlockSelector;

			},

			toggleTo : function(n){

				/**
				 * toggleTo(n)
				 *
				 * @param {Number} n
				 *
				 * @return {Undefined}
				 */

				var e = $(this.options.menuSelector, this.container).get(n);

				if(e){

					this.toggleBy(e);

				}else{

					throw new Error(ERROR_NOT_ITEM);

				}

			},

			getCurrent : function(){

				/**
				 *
				 * getCurrent()
				 *
				 * @return {Array}
				 *
				 */

				var data = this.data;

				return [data.curMenu, data.curBlock, data.curSelector];

			},

			getPrevious : function(){

				/**
				 *
				 * getPrevious()
				 *
				 * @return {Array}
				 *
				 */

				var data = this.data;

				return [data.prevMenu, data.prevBlock, data.prevSelector];

			},

			getOptions : function(name){

				/**
				 *
				 * getOptions([,name])
				 *
				 * @param {String} name
				 *
				 * @return {*}
				 *
				 */

				var options = this.options;

				return typeof name == "string" ? options[name] : $.extend({}, options);

			},

			setOptions : function(name, value){

				/**
				 * setOptions(name, value);
				 *
				 * @param {String} name
				 * @param {*} value
				 *
				 * @return {Undefined}
				 */

				var options = this.options;

				if(typeof name == "string"){

					options[name] = value;

				}else if(name instanceof Object){

					$.extend(options, name);

				}else{

					throw new Error(ERROR_NOT_OBJECT);

				}

			},

			destroy : function(userHandlers){

				/**
				 * destroy([userHandlers])
				 *
				 * @param {Boolean} userHandlers
				 *
				 * @return {Undefined}
				 *
				 */

				var $element = $(this.element);

				$element
					.off(this.options.event, _JSTabsConstructor.handler)
					.removeData(_JSTabsConstructor.ID);

				if(userHandlers){

					$element
						.off(TRIGGER_AFTER_CHANGE + "." + TRIGGER_PREFIX)
						.off(TRIGGER_BEFORE_CHANGE + "." + TRIGGER_PREFIX)
						.off(TRIGGER_BEFORE_SCAN + "." + TRIGGER_PREFIX);

				}else{

					$element.off(TRIGGER_AFTER_CHANGE + "." + TRIGGER_PREFIX, _trigger);

				}

			},

			rebuild : function(options){

				/**
				 * rebuild([options])
				 *
				 * @param {Object} options
				 *
				 * @return {Undefined}
				 */

				this.destroy(false);

				_JSTabsConstructor.init.call($(this.element), options);

			}

		};
		
		
		$.jsTabs = {
			handlers : {
				AFTER_CHANGE  : TRIGGER_AFTER_CHANGE + "." + TRIGGER_PREFIX,
				BEFORE_CHANGE : TRIGGER_BEFORE_CHANGE + "." + TRIGGER_PREFIX,
				BEFORE_SCAN   : TRIGGER_BEFORE_SCAN + "." + TRIGGER_PREFIX
			}
		};

		////////////////////////////////////////
		return function(){
			return _JSTabsConstructor.init.apply(this, arguments);
		};
		////////////////////////////////////////

	}

);