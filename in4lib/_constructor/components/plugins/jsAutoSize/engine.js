//Пока что не используется

!IN4_BASE.addPlugin("jsAutoSize",
	{},
	function(){
		
		function AutoResizeOptions(element, axis, cf){

			/**
			 * AutoResizeOptions(element, axis, cf);
			 *
			 * @constructor {AutoResizeOptions}
			 * 
			 * @param {Element} element
			 * @param {String} axis
			 * @param {Number} cf
			 *
			 */

			this.cf      = cf;
			this.element = element;
			this.axis    = axis;
			
		}

		AutoResizeOptions.prototype = {
			
			constructor : AutoResizeOptions,
			
			resize : function(){

				/**
				 * resize()
				 * 
				 * @return {Undefined}
				 */

				var $element = $(this.element);

				if(this.axis == "X"){

					$element.width( $element.height() * data.cf );

				}else{

					$element.height( $element.width() / data.cf );

				}
				
			}
			
		};

		var sizeDump = [], $window = $(window);

		$window.on("resize", function(){

			function _resize(data){

				/**
				 * _resize(data)
				 *
				 * @param {AutoResizeOptions} data
				 *
				 * @return {Undefined}
				 */

				data.resize();

			}

			sizeDump.forEach(_resize);

		});		

		return function(pW, pH, orientation){

			/**
			 * function(pW, pH, orientation);
			 * 
			 * @param {Number} pW
			 * @param {Number} pH
			 * @param {String} orientation
			 * 
			 * @return {jQuery}
			 */

			function _init(){

				/**
				 * _init()
				 * 
				 * @return {Undefined}
				 */

				sizeDump.push(
					new AutoResizeOptions(this, orientation, pW / pH)
				);

			}

			return this.each(_init), _onResize(), this;

		};

	}
);