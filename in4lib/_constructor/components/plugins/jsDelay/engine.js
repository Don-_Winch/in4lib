/**
 * $jQElem.jsDelay(time, func)
 *
 * Более удобный setTimeout для jQuery
 *
 * @param {Number} time - время, через которое сработает таймер
 * @param {Function} func - функция, вызываемая по истечению времени time. Вызывается как func.call($jQElem)
 * @return {jQuery} - $jQElem
 */

!IN4_BASE.addPlugin("jsDelay",
	{
		defaultOptions : { delay : 1 }
	},
	function(_ops){

		return function(time, f){

			var $targets = this;

			setTimeout(function(){
				f.call($targets);
			}, time || _ops.defaultOptions.delay);

			return this;
		}

	}
); 