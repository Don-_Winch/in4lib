<?php

define("COMPONENTS_DIR", "components/");
define("PLUGINS_DIR", COMPONENTS_DIR . "plugins/");
define("ENGINE_DIR", COMPONENTS_DIR . "engine/");
define("JS_ENGINE_FILE_MIN", "engine.min.js");
define("JS_ENGINE_FILE_NORMAL", "engine.js");
define("JS_MANIFEST_FILE", "manifest.json");

?>